<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 14/04/2015
 * Time: 15:01
 */
namespace Skimia\Pages\Fragments;

use Skimia\Auth\Traits\Acl;

trait FragmentContainerTrait {

    use Acl;
    //protected $fragments = null;

    protected $arrayFragments = null;

    public function getFragments(){
        if(empty($this->fragments))
            return [];
        elseif(empty($this->arrayFragments))
            $this->arrayFragments = unserialize($this->fragments);

        return $this->arrayFragments;

    }

    public function setFragments($fragments){
        $this->arrayFragments = $fragments;
        $this->fragments = serialize($fragments);
    }

    public function renderFragment($type, $identifier, $mergecfg = []){
        $config = $this->getFragment($identifier);

        switch($type){
            case 'section':
                $fragment = new SectionFragment($config,$identifier,$this);
                break;
            case 'endsection':
                $fragment = new EndSectionFragment($config,$identifier,$this);
                break;
            case 'text':
                $fragment = new TextFragment($config,$identifier,$this);
                break;
            case 'tiny_text':
                $fragment = new TinyTextFragment($config,$identifier,$this);
                break;
            case 'image':
                $fragment = new ImageFragment($config,$identifier,$this);
                break;
            case 'link':
                $fragment = new LinkFragment($config,$identifier,$this);
                break;
        }

        return $fragment->render($mergecfg,$this->liveEdit());
    }

    protected $liveEdit = null;

    public function getLiveEdit(){
        return $this->liveEdit();
    }

    protected function liveEdit(){

        if($this->liveEdit == null)
            $this->liveEdit = $this->getAcl()->can('live_edit');

        return $this->liveEdit;
    }
    public function getFragment($identifier){

        if($this->hasFragment($identifier))
            return $this->arrayFragments[$identifier];
        else
            return [];
    }

    public function hasFragment($identifier){
        if($this->arrayFragments == null){
            $this->getFragments();
        }
        return isset($this->arrayFragments[$identifier]);
    }

    public function fragGetTable(){
        return $this->table;
    }
}