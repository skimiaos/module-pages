<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 14/04/2015
 * Time: 15:17
 */

namespace Skimia\Pages\Fragments;


class TinyTextFragment extends Fragment{

    protected $template      = 'skimia.pages::fragments.text';
    protected $live_template = 'skimia.pages::fragments.live_tiny_text';

    protected function getDefaultConfig(){
        return [
            'text'=>'Lorem impsum',
        ];
    }

    protected function getFormFields(){
        return [
            'text'=>['type'=>'text','label'=>'contenu']
        ];
    }
}