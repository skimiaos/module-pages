<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 14/04/2015
 * Time: 15:17
 */

namespace Skimia\Pages\Fragments;


class TextFragment extends Fragment{

    protected $template      = 'skimia.pages::fragments.text';
    protected $live_template = 'skimia.pages::fragments.live_text';

    protected function getDefaultConfig(){


        $phrases = [
            "<p>It was Passepartout himself, who had slipped upon the pyre in the midst of the smoke and, profiting by the still overhanging darkness, had delivered the young woman from death! It was Passepartout who, playing his part with a happy audacity, had passed through the crowd amid the general terror.</p>",
            "<p>A moment after all four of the party had disappeared in the woods, and the elephant was bearing them away at a rapid pace. But the cries and noise, and a ball which whizzed through Phileas Fogg's hat, apprised them that the trick had been discovered.</p>",
            "<p>The old rajah's body, indeed, now appeared upon the burning pyre; and the priests, recovered from their terror, perceived that an abduction had taken place. They hastened into the forest, followed by the soldiers, who fired a volley after the fugitives; but the latter rapidly increased the distance between them, and ere long found themselves beyond the reach of the bullets and arrows.</p>",
            "<p>The station at Allahabad was reached about ten o'clock, and, the interrupted line of railway being resumed, would enable them to reach Calcutta in less than twenty-four hours. Phileas Fogg would thus be able to arrive in time to take the steamer which left Calcutta the next day, October 25th, at noon, for Hong Kong.</p>",
            "<p>Passepartout made it a point, as he made his purchases, to take a good look at the city. It was formerly defended by a noble fort, which has since become a state prison; its commerce has dwindled away, and Passepartout in vain looked about him for such a bazaar as he used to frequent in Regent Street.</p>",
            "<p>The Lion was angry at this speech, but could say nothing in reply, and while he stood silently gazing at the Ball of Fire it became so furiously hot that he turned tail and rushed from the room. He was glad to find his friends waiting for him, and told them of his terrible interview with the Wizard.</p>",
            "<p>It was the White Rabbit, trotting slowly back again, and looking anxiously about as it went, as if it had lost something; and she heard it muttering to itself 'The Duchess! The Duchess! Oh my dear paws! Oh my fur and whiskers! She'll get me executed, as sure as ferrets are ferrets! Where CAN I have dropped them, I wonder?' Alice guessed in a moment that it was looking for the fan and the pair of white kid gloves, and she very good-naturedly began hunting about for them, but they were nowhere to be seen—everything seemed to have changed since her swim in the pool, and the great hall, with the glass table and the little door, had vanished completely.</p>",
            "<p>Very soon the Rabbit noticed Alice, as she went hunting about, and called out to her in an angry tone, 'Why, Mary Ann, what ARE you doing out here? Run home this moment, and fetch me a pair of gloves and a fan! Quick, now!' And Alice was so much frightened that she ran off at once in the direction it pointed to, without trying to explain the mistake it had made.</p>",

        ];


        return [
            'text'=> $phrases[array_rand($phrases)],
        ];
    }

    protected function getFormFields(){
        return [
            'text'=>['type'=>'text','label'=>'contenu']
        ];
    }
}