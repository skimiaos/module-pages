<?php

namespace Skimia\Pages\Fragments;


class ImageFragment extends Fragment{

    protected $template      = 'skimia.pages::fragments.image';
    protected $live_template = 'skimia.pages::fragments.live_image';

    protected function getDefaultConfig(){
        return [
            'src' => 'http://lorempixel.com/1920/1080/',
            'description' => 'Random test image',
        ];
    }

    protected function getFormFields(){
        return [
            'src'=>['type'=>'text','label'=>'Source'],
            'description'=>['type'=>'text','label'=>'Description']
        ];
    }
}