<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 14/04/2015
 * Time: 15:14
 */

namespace Skimia\Pages\Fragments;


abstract class Fragment {

    protected $config;
    protected $template;
    protected $live_template;
    protected $identifier;
    protected $fixed_entity;

    public function __construct($config, $identifier, $fixed_entity){

        $this->config = array_merge($this->getDefaultConfig(),$config);
        $this->identifier = $identifier;
        $this->fixed_entity = $fixed_entity;
        return $this;
    }

    protected abstract function getDefaultConfig();
    protected abstract function getFormFields();

    public function render($mergeConfig = [], $live = false){
        $mergeConfig = array_merge($mergeConfig,[
            'identifier'=>$this->identifier,
            'table'=>$this->fixed_entity->fragGetTable(),
            'id'=>$this->fixed_entity->id
        ]);
        return \View::make((!$live ? $this->template:$this->live_template),$this->config,$mergeConfig);
    }
}