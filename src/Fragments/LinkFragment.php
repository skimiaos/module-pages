<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 14/04/2015
 * Time: 15:17
 */

namespace Skimia\Pages\Fragments;


class LinkFragment extends Fragment{

    protected $template      = 'skimia.pages::fragments.link';
    protected $live_template = 'skimia.pages::fragments.live_link';

    protected function getDefaultConfig(){

        return [
            'text'=>'Lien',
            'href'=>'#',
            'title'=>''
        ];
    }

    protected function getFormFields(){
        return [
            'text'=>[
                'type'=>'text',
                'label'=>'texte'
            ],
            'href'=>[
                'type'=>'text',
                'label'=>'adresse du lien'
            ],
            'title'=>[
                'type'=>'text',
                'label'=>'titre du lien'
            ]
        ];
    }
}