<?php

namespace Skimia\Pages;

require module_lib('skimia.pages','scssphp','scss.inc.php');

use League\Flysystem\Exception;
use Skimia\Blade\Facades\BladeHelper;
use Skimia\Modules\ModuleBase;
use BackendNavigation as Nav;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Skimia\Pages\Data\Models\ComponentPage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Theme;
use App;


class Module extends ModuleBase{

    public function preBoot(){
        $this->app->bindShared('skimia.pages.managers.pagecontainer', function(){
            return new Managers\PageContainer();
        });

        $this->app->bindShared('skimia.pages.managers.pages', function(){
            return new Managers\Pages();
        });


        BladeHelper::createFunction('sLiveSection', null, '<?php if($file->getLiveEdit()) echo "live-section=$1"; ?>');

        BladeHelper::createFunction('LiveSection', null, '<?php if($file->getLiveEdit()) echo "<div live-section=$1>"; ?>');

        BladeHelper::createTag('EndLiveSection', '<?php if($file->getLiveEdit()) echo "</div>"; ?>');

        BladeHelper::createFunction('TeXt',null,'<?php echo $file->renderFragment(\'text\',$1); ?>');

        BladeHelper::createFunction('TinyText',null,'<?php echo $file->renderFragment(\'tiny_text\',$1); ?>');

        BladeHelper::createFunction('Image',null,'<?php echo $file->renderFragment(\'image\',$1); ?>');

        BladeHelper::createFunction('Link',null,'<?php echo $file->renderFragment(\'link\',$1); ?>');

        BladeHelper::createFunction('Component',null,'<?php echo $file->getComponent($1)->renderShow($file); ?>');

        BladeHelper::createFunction('Partial',null,'<?php echo Pages::renderPartial($1); ?>');
    }

    public function register(){
        parent::register();

        /*\Event::listen('os.asset.save',function( $asset, $path ){
            if(ends_with($path,'.scss')){

                $files = \File::allFiles(\Assets::getPrivateAssetDir().'/scss/');
                $count_files = 0;
                foreach($files as $file){
                    $filename = (string)$file;
                    if(ends_with($filename,'.scss') && !starts_with(basename($filename),'_') ){
                        $scss = new \scssc();
                        $scss->addImportPath(module_libs('skimia.pages','compass/frameworks/compass/stylesheets'));
                        $scss->addImportPath(module_libs('skimia.pages','compass/frameworks/blueprint/stylesheets'));
                        $scss->addImportPath(dirname($filename));
                        $css =  $scss->compile(\File::get($filename));
                        $path = str_replace('scss','css',$filename);
                        if(!\File::exists(dirname($filename))){
                            \File::makeDirectory(dirname($filename), 0777, true);
                        }
                        \File::put($filename, $css);
                        $count_files++;
                    }
                }
                if($count_files > 0)
                    \AResponse::addMessage($count_files . ' Fichier'.($count_files > 1 ? 's':'').' recompilés');
            }





        });*/

        App::missing(function($exception)
        {
            return App::make('Skimia\Pages\Controllers\Pages')->error404();
        });

        App::error(function(ModelNotFoundException $exception)
        {
            return App::make('Skimia\Pages\Controllers\Pages')->error404();
        });
    }

    public function getAliases(){
        return [
            'ComponentManager' => 'Skimia\Pages\Facades\ComponentManager',
            'Pages'            => 'Skimia\Pages\Facades\Pages',
            'Widgets'            => 'Skimia\Pages\Facades\Widget'
        ];
    }

    public function postBoot(){

        if( php_sapi_name() != 'cli') {
            $pagesComponents = ComponentPage::all(['url', 'alias', 'template_id', 'fragments']);

            foreach ($pagesComponents as $page) {
                \Route::any($page->url, ['as' => 'skimia.pages::page-component.' . $page->alias,
                    'uses' => function () use ($page) {

                        return App::make('Skimia\Pages\Controllers\Pages')->componentPage($page);


                    }
                ]);
            }
        }
        \Route::any('/',array('as'=>'skimia.pages::page_home','uses'=>'Skimia\Pages\Controllers\Pages@home'));
        \Route::any('/{pageUrl}', array('as'=>'skimia.pages::page_link','uses'=>'Skimia\Pages\Controllers\Pages@page') )->where('pageUrl', '[\.0-9A-Za-z\-/]+');



    }

} 