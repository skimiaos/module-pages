<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 15:15
 */
namespace Skimia\Pages\Components\Demo;

use Skimia\Pages\Components\Component;

class DemoComponent extends Component{

    protected static $systemName = 'pages_demo';

    protected $name = 'demo';
    protected $description = 'bah en fait... juste utilisé pour tests';
    protected $icon = 'os-icon-box-2';

    protected $show_template = 'skimia.pages::components.demo.show';


    protected $fields  = [
        'name'=>[
            'type'=>'text',
            'label'=>'Votre nom',
            'default'=>'World',
            'required'
        ]
    ];
}