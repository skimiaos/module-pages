<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 13:41
 */

namespace Skimia\Pages\Components;

use ComponentManager as CManager;

use Skimia\Pages\Data\Models\ComponentPosition;

trait ComponentContainerTrait {

    protected $components = false;

    public function getComponents(){
        if($this->components === false)
        {
            $components = ComponentPosition::where('entity_type','=',$this->getTable())->where('entity_id','=',$this->id)->get();
            $this->components = [];
            foreach($components as $value)
            {
                $class = CManager::getComponentClass($value->component_id);
                $this->components[$value->identifier] = new $class($value);
            }

        }
        return $this->components;
    }

    public function hasComponent($identifier){
        if($this->components  === false){
            $this->getComponents();
        }
        if(isset($this->components[$identifier]))
            return true;
        return false;
    }

    /**
     * @param $identifier
     * @return Component
     */
    public function getComponent($identifier){
        if($this->components  === false){
            $this->getComponents();
        }
        if(isset($this->components[$identifier]))
            return $this->components[$identifier];


        if($this->hasParent()){
            return $this->getParentComponentContainer()->getComponent($identifier);
        }


        return new \AnObj([
            'renderShow'=> function()use( $identifier){
                return 'component '.$identifier.' introuvable';
            }
        ]);
    }

    public function hasParent(){
        return method_exists($this,'getParentComponentContainer');
    }
    /**
     * @param $name
     * @param $identifier
     * @return Component
     */
    public function addComponent($name,$identifier){

        if($this->hasComponent($identifier)){
            throw new \Exception('un composant existe deja sur cet identifiant');
        }

        $this->components[$identifier] = CManager::getNewComponent($name,$this->getTable(),$this->id,$identifier);
        return $this->components[$identifier];
    }

    public function getTable(){
        return $this->table;
    }

    public function unBindComponent($identifier){


        $this->getComponent($identifier)->getPosition()->delete();
        unset($this->components[$identifier]);
        return $this;
    }

    public function describeComponents(){
        if($this->components  === false){
            $this->getComponents();
        }

        $descriptions = [];
        foreach($this->components as $comp) {
            $descriptions[] = [
                'identifier'=>$comp->getPosition()->identifier,
                'info' => $comp->getDescriptionData(),
                'template' => $comp->renderForm(),
                'form' => $comp->onForm()
            ];
        }
        return $descriptions;
    }

}
