<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 04/03/2015
 * Time: 18:12
 */
namespace Skimia\Pages\Components;

use Skimia\Angular\Form\AngularTrait;
use Skimia\Form\Base\Form;
use Skimia\Pages\Data\Models\ComponentPage;
use Skimia\Pages\Data\Models\ComponentPosition;

class Component extends Form{

    use AngularTrait;
    /**
     * @var ComponentPosition
     */
    protected $position = null;

    protected static $systemName = 'component';
    protected static $page = false;

    public static function getSYSName(){
        return static::$systemName;
    }

    public static function getPage(){
        return static::$page;
    }

    public function makeId(){
        return md5(
            $this->position->entity_type.$this->position->entity_id.$this->position->identifier
        );
    }
    public function __construct($entity, $entityId = false, $identifier = false){
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        if($entity === false){
            return parent::__construct();
        }
        if(is_object($entity) && (is_a($entity,'Skimia\Pages\Data\Models\ComponentPosition')||is_subclass_of($entity,'Skimia\Pages\Data\Models\ComponentPosition'))){
            $this->position = $entity;
            $this->position->setComponent($this);
        }else{
            if(is_string($entity) && is_integer($entityId)&& is_string($identifier)){
                $this->position = new ComponentPosition(static::$systemName, $entity,$entityId,$identifier);
                $this->position->setComponent($this);
                $this->position->setConfiguration($this->getDefaultConfiguration());
                $this->position->save();
            }else{
                dd(get_defined_vars());
                //TODO
                throw new \Exception('TODO');
            }
        }
        parent::__construct();
    }

    /*** DESCRIPTION ***/

    protected $name = 'Nouveau composant';
    protected $description = 'il ne fait rien';
    protected $icon = '';//@fontawesome sans fa-{icon}


    public function getDescriptionData(){
        return [
            'name'=>$this->name,
            'description'=>$this->description,
            'icon'=>$this->icon,
            'page'=>static::getPage()
        ];
    }


    /***  VUES  ***/
    protected $javascript_template = false;
    protected $form_template = false;
    protected $show_template = false;
    //protected $template = 'skimia.pages::components.form_theme';

    protected $container = null;

    public function getJavascriptTemplate(){
        return $this->javascript_template;
    }

    public function getFormTemplate(){
        return $this->form_template;
    }

    /**
     * generation
     * @return bool
     */
    public function getShowTemplate(){
        if(isset($this->container)){
            //determinate view;
            return $this->determineView($this->container);

        }else{
            return $this->show_template;
        }

    }

    public function getShowFile(){

        return  \File::get(\View::make($this->getShowTemplate())->getPath());
    }


    /***  GENERATION VARIABLES POUR LES VUES ***/

    public function onJavascript($merge_config = array()){//autoriser simplement d'ajouter des variables avec parent::onJavascript

        $config = $this->position->getConfiguration();//VA chercher dans l'entité
        $config['__id'] = $this->makeId();
        return array_merge($config,$merge_config);
    }

    public function onForm($merge_config = array()){//autoriser simplement d'ajouter des variables avec parent::onJavascript

        $config = $this->position->getConfiguration();//VA chercher dans l'entité
        $conf = array_merge($config,$merge_config,['_identifier'=>$this->position->identifier]);

        $this->makeFieldsIfNotMaked();

        foreach ($this->fields as $key=>$definition ) {

            if (\AngularFormHelper::hasDataViewTransformer($definition['type'])) {
                \AngularFormHelper::TransformDataToView($definition['type'], [&$conf, &$conf[$key], $key, $definition, $this], true);
            }
        }
        //dd($conf);
        return $conf;
    }

    public function onShow($merge_config = array()){//autoriser simplement d'ajouter des variables avec parent::onJavascript

        $config = $this->position->getConfiguration();//VA chercher dans l'entité
        return array_merge($config,$merge_config);
    }


    /*** GENERATION DE L'HTML ***/

    /**
     * @param ComponentContainerTrait|null $container
     * @return string
     */
    public function renderShow( $container = null){
        $data = $this->onShow([]);

        $links = [];
        $this->makeFieldsIfNotMaked();
        foreach ($this->fields as $k=>$field) {
            if($field['type'] == 'component-page-link'){
                $links[$k] = function($params = []) use($k,$field,$data){
                    if(!isset($data[$k]))
                        $cp = ComponentPage::findForceBySysName($field['sysName'])->first();
                    else
                        $cp = ComponentPage::find($data[$k]);
                    return component_route($cp->alias,$params);
                };
            }
        }
        $data['_links'] = new \AnObj($links);

        $data['__id'] = $this->makeId();


        if(isset($container)){

            //determinate view;
            $this->container = $container;

            $data = array_merge($data,['file'=>$this->container]);
        }
        $path = $this->position->getFileTemplate();
        return \View::make($path,$data)->render();
    }

    public function determineView($container){

        if(is_a($container,'\Skimia\Pages\Data\Models\ComponentPage')){
            //dd(sprintf('pages.components.%s.%s.%s','pages_components', str_replace('.','-',$container->alias),$this->position->identifier));
            if(\View::exists(sprintf('pages.components.%s.%s.%s','pages_components', str_replace('.','-',$container->alias),$this->position->identifier))){
                return sprintf('pages.components.%s.%s.%s','pages_components', str_replace('.','-',$container->alias),$this->position->identifier);
            }else{
                $container = $container->template;
            }
        }

        $path = is_a($container,'\Skimia\Pages\Data\Models\Page')?str_replace(['.'],'-',$container->template->path):str_replace('.','-',$container->path);

        //dd(\View::exists(sprintf('pages.components.%s',$this->getSYSName())));
        if(\View::exists(sprintf('pages.components.%s.%s.%s',$this->position->entity_type, $path,$this->position->identifier))){
            return sprintf('pages.components.%s.%s.%s',$this->position->entity_type, $path,$this->position->identifier);
        }
        /*else if($this->position->entity_type == 'pages_pages' && \View::exists(sprintf('pages.components.pages.%s.%s.%s',$this->position->entity_type, str_replace('.','-',$container->path),$this->getSYSName()))){
            return sprintf('pages.components.pages.%s.%s.%s',$this->position->entity_type, str_replace('.','-',$container->path),$this->getSYSName());
        }*/
        else if($this->position->entity_type != 'pages_pages' && \View::exists(sprintf('pages.components.%s.%s.%s',$this->position->entity_type, $path,$this->getSYSName()))){
            return sprintf('pages.components.%s.%s.%s',$this->position->entity_type, $path,$this->getSYSName());
        }
        else if(\View::exists(sprintf('pages.components.%s.%s',$this->position->entity_type,$this->getSYSName()))){
            return sprintf('pages.components.%s.%s',$this->position->entity_type,$this->getSYSName());
        }
        else if(\View::exists(sprintf('pages.components.%s',$this->getSYSName()))){
            return sprintf('pages.components.%s',$this->getSYSName());
        }
        else{
            return $this->show_template;
        }

    }

    public function renderForm(){

        //$data = $this->onForm([]);

        return $this->render('asubmit',
            $this->getFormTemplate() === false ?
                $this->template
                : $this->getFormTemplate()
        );
    }

    public function renderJavascript(){
        if($this->getJavascriptTemplate() === false)
            return '';
        $data = $this->onJavascript([]);

        return \View::make($this->getJavascriptTemplate(),$data)->render();
    }


    /*** CONFIGURATION ***/
    public function getDefaultConfiguration(){
        $conf = [];
        $this->makeFieldsIfNotMaked();
        foreach($this->fields as $name=>$field){
            if($name !== '_identifier' && isset( $field['default'] ))
                $conf[$name] = $field['default'];
        }
        return $conf;
    }

    public function getPosition(){
        return $this->position;
    }

    public function savePosition(){
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        if($this->isValid()) {
            $config = $this->all()->toArray();
            $identifier = $config['_identifier'];
            unset($config['_identifier']);
            $this->position->identifier = $identifier;
            //TODO moove template
            $this->position->setConfiguration($config);
            $this->position->save();
        }
    }

    protected static function getNew(){
        return new static(false);
    }

    public function getStaticJS(){return '';}
    public function getDynJS(){return '';}

    public function getStaticCSS(){return '';}
    public function getDynCSS(){return '';}
}
