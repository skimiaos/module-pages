<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 04/03/2015
 * Time: 18:12
 */
namespace Skimia\Pages\Components;

use Skimia\Pages\Data\Models\ComponentPage;

class ComponentManager {

    protected $components_aliases = [];
    protected $component_pages_definitions = [];

    public function getNewComponent($name,$entity,$id,$identifier){
        $class = $this->components_aliases[$name];
        return new $class($entity,$id,$identifier);
    }

    public function getComponent($name,$entity){
        $class = $this->components_aliases[$name];
        return new $class($entity);
    }

    public function register($class){
        $this->components_aliases[$class::getSYSName()] = $class;
        if($class::getPage() !== false)
            $this->component_pages_definitions[ $class::getSYSName() ] = $class::getPage();
    }

    public function has($name){
        return isset($this->components_aliases[$name]);
    }

    public function getComponentClass($name){
        return $this->components_aliases[$name];
    }

    public function describeComponents(){
        $descriptions = [];
        foreach($this->components_aliases as $alias=>$component){
            $class = new $component(false);
            $descriptions[$alias] = $class->getDescriptionData();
        }
        return $descriptions;
    }

    public function getPageDefinition( $name ){
        return $this->component_pages_definitions[$name];
    }

    public function createComponentPage($systemName, $templateid = null, $alias = null){
        $definition = $this->getPageDefinition($systemName);
        $cp = new ComponentPage();

        $cp->url = $definition['url'];
        $cp->system_name = $systemName;
        if($templateid)
            $cp->template_id = $templateid;

        if(!$alias)
            $alias = $systemName;
        $cp->alias = $alias;

        $cp->save();
        return $cp;
    }
}