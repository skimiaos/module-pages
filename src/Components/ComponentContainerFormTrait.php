<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 12/03/2015
 * Time: 20:55
 */
namespace Skimia\Pages\Components;



trait ComponentContainerFormTrait {

    protected function prepareGetEdit($form){
        return array_merge([
            'components' => $this->data->describeComponents()
        ],
            $form);
    }
}