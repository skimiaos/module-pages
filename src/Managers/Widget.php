<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 01/04/2015
 * Time: 17:11
 */

namespace Skimia\Pages\Managers;

use Illuminate\Support\Collection;
use Skimia\Pages\Data\Models\ComponentPage;
use View;
use Skimia\Pages\Data\Models\Page;
use Skimia\Pages\Data\Models\Partial;

require_once module_lib('skimia.pages','simple_html_dom.php');
class Widget {

    protected $_widgetsLoaded = false;
    protected $_widgets = null;

    function __construct(){

        $this->_widgets = new Collection();
    }

    public function loadWidgets($path = false){
        $this->implementsBlade();

        if(!$path ){
            $path = realpath(\Theme::getDefaultTheme()['path'].'/widgets');
        }

        if(\File::exists($path)){

            $files = \File::allFiles($path);
            $uniqNamespace = uniqid('widgets_');
            View::addNamespace($uniqNamespace, $path);
            foreach ($files as $splFileInfo) {

                $this->loadWidget($uniqNamespace,$splFileInfo->getFileName(),str_replace(['/','\\'],'.',trim(str_replace($path,'',$splFileInfo->getPath()),'\\/')));
            }
            $this->_widgetsLoaded = true;
        }



    }

    public function mergeData($widgetName, $new){
        $current = $this->_widgets->get($widgetName);
        $current = array_merge($current,$new);

        $this->_widgets->put($widgetName,$current);
    }


    protected function loadWidget($uniqNamespace, $fileName, $path){


        $widgetNameParts = explode('.',$fileName);
        $widgetName = implode('.',array_slice($widgetNameParts,0, count($widgetNameParts) -2));

        $this->_widgets->put($widgetName,[]);

        $dom = View::make($uniqNamespace.'::'.$path.'.'.$widgetName,['widgetName'=>$widgetName])->render();

        $this->annalyseDom($widgetName,$dom);
    }

    protected function annalyseDom($widgetName,$dom){

        $html = str_get_html($dom);
        /**
         * @var $rootNode \simple_html_dom_node
         */
        $rootNode = $html->firstChild();

        $editables = $rootNode->find('*[widget-edit]');


        $edit = [];
        foreach ($editables as $htmlNode) {
            $selector = $this->makeSelector($htmlNode);
            $id = $this->alphanum($selector);
            $edit[$id] = [
                'selector'=>  $selector,
            ];
            $editType = $htmlNode->getAttribute('widget-edit');
            if($editType != 'full'){
                $edit[$id]['allowedContent'] = $this->allowedContents($editType);
            }
        }

        $parts = $rootNode->find('*[widget-part]');

        $partsEdit = [];
        foreach ($parts as $htmlNode) {
            $selector = $this->makeSelector($htmlNode);
            $id = $this->alphanum($selector);
            $partsEdit[$id] = $selector;

        }

        $allowed = [];
        $elements = $rootNode->find('*[class]');
        foreach ($elements as $elem) {
            $allowed[$elem->tag] = $elem->tag;
        }



        $this->mergeData($widgetName,[
            'rootNode'=>[
                'tag'=>$rootNode->tag,
                'class'=>$rootNode->getAttribute('class')
            ],
            'editables'=>$edit,
            'allowedContent'=> $allowed,
            'dom'=>$dom,
            'parts'=>$partsEdit
        ]);

    }


    protected function makeSelector(\simple_html_dom_node $htmlNode){
        $tag = $htmlNode->tag;
        $class = $htmlNode->getAttribute('class');
        if($class == false)
            return $tag;
        $class = '.'.str_replace(' ','.',$class);
        return $tag.$class;
    }

    protected function alphanum($string){
        $string = preg_replace("/[^0-9a-zA-Z ]/m", "", $string);
        return preg_replace("/ /", "-", $string);
    }


    protected function allowedContents($type){
        switch($type){
            case 'tiny':
                return 'br strong em span';
                break;
        }
    }
    protected function implementsBlade(){
        \BladeHelper::createFunction('widgetButton',null,'<?php Widgets::mergeData($widgetName,[\'button\'=>$1]); ?>');

        \BladeHelper::createTag('wEdit',' widget-edit="full" ');
        \BladeHelper::createTag('wTinyEdit',' widget-edit="tiny" ');

        \BladeHelper::createTag('pEdit',' widget-part="full" ');
    }

    public function makeWidgets(){

        $output = '';
        foreach ($this->_widgets as $name=>$widget) {
            $output .= "\n".$this->makeWidget($name);
        }


        return $output;


    }

    protected function makeWidget($widgetName){

        $data = $this->_widgets->get($widgetName);
        $rendered = View::make('skimia.pages::ckeditor.widget',['name'=>$widgetName,'widget'=>$data])->render();
        return $rendered;
    }

    public function makePlugin($pluginName){
        if(!$this->_widgetsLoaded)
            $this->loadWidgets();

        $widgets = $this->makeWidgets();

        $rendered = View::make('skimia.pages::ckeditor.plugin',[
            'name'=>$pluginName,
            'widgets_names'=>$this->_widgets->keys(),
            'widgets'=>$widgets
        ])->render();

        return $rendered;
    }
}