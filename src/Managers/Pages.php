<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 01/04/2015
 * Time: 17:11
 */

namespace Skimia\Pages\Managers;

use Illuminate\Support\Collection;
use Skimia\Pages\Data\Models\ComponentPage;
use View;
use Skimia\Pages\Data\Models\Page;
use Skimia\Pages\Data\Models\Partial;

class Pages {

    protected $rawCSS = '';
    protected $rawJS = '';


    public function addRAWJS($raw){
        $this->rawJS .= $raw;
        return $this;
    }

    public function addRAWCSS($raw){
        $this->rawCSS .= $raw;
        return $this;
    }

    public function getRAWCSS(){
        return $this->rawCSS;
    }

    public function getRAWJS(){
        return $this->rawJS;
    }

    /**
     * Retourne le code rendu d'une page
     * @param Page $page
     * @return string
     */
    public function renderPage(Page $page){

        $layout = $page->template()->getResults()->layout()->getResults();
        if(isset($layout))
            return View::make('skimia.pages::render_page_layout',[
                'page'=>$page,
                'template' => $page->template()->getResults(),
                'layout'=>$layout,
            ])->render();
        else
            return View::make('skimia.pages::render_page',[
                'page'=>$page,
                'template' => $page->template()->getResults(),
            ])->render();

    }

    public function renderPartial($name){
        $partial = Partial::where('path', $name)->first();

        if(isset($partial) && View::exists('pages.partials.'.str_replace('.','---',$partial->path)))
            return View::make('pages.partials.'.str_replace('.','---',$partial->path),['file'=>$partial])->render();
        elseif(!isset($partial))
            return 'Partial : '.$name.' introuvable';
        else
            return 'Vue : '.$partial->path.' introuvable';
    }

    public function renderComponentPage(ComponentPage $page){
        $template = $page->template()->getResults();
        if(!$template)
            return 'Impossible d\'afficher cette page il faut définir une template au component Route alias "'.$page->alias.'""';
        return View::make('skimia.pages::render_page_layout',[
            'page'=>$page,
            'template' => $template,
            'layout'=>$template->layout()->getResults(),
        ])->render();
    }
}