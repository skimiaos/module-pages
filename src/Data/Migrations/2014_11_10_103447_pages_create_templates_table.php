<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Skimia\Pages\Data\Models\Layout;
use Skimia\Pages\Data\Models\Template;
class PagesCreateTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('pages_templates')){
            Schema::create('pages_templates', function($table){
                $table->increments('id');
                $table->string('path')->unique();
                $table->text('description')->nullable();
                $table->unsignedInteger('layout_id');

                $table->timestamps();

            });
        }

        $layout = new Layout();
        $layout->path = 'default.htm';
        $layout->description = 'Default layout';
        $layout->upd_files =false;
        $layout->save();

        $template = new Template();
        $template->path = 'homepage.htm';
        $template->description = 'page d\'accueil';
        $template->layout_id = $layout->id;
        $template->upd_files =false;
        $template->save();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::drop('pages_templates');
	}

}
