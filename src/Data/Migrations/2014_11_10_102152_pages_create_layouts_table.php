<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PagesCreateLayoutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('pages_layouts')){
            Schema::create('pages_layouts', function(BluePrint $table){
                $table->increments('id');
                $table->string('path')->unique();
                $table->text('description')->nullable();

                $table->text('fragments')->nullable();

                $table->timestamps();
            });
        }

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pages_layouts');
	}

}
