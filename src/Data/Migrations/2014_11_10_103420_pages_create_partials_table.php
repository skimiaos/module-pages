<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Skimia\Pages\Data\Models\Partial;
class PagesCreatePartialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('pages_partials')){
            Schema::create('pages_partials', function($table){
                $table->increments('id');
                $table->string('path')->unique();
                $table->text('description')->nullable();

                $table->text('fragments')->nullable();

                $table->timestamps();
            });
        }

		$partial = new Partial();
		$partial->path = 'header.htm';
		$partial->description = 'Default Header';
		$partial->upd_files =false;
		$partial->save();

		$partial = new Partial();
		$partial->path = 'leftmenu.htm';
		$partial->description = 'Default LeftMenu';
		$partial->upd_files =false;
		$partial->save();

		$partial = new Partial();
		$partial->path = 'footer.htm';
		$partial->description = 'Default Footer';
		$partial->upd_files =false;
		$partial->save();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages_partials');
	}

}
