<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComponentsPages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('pages_component')){

			Schema::create('pages_component', function($table){
				$table->increments('id');

				$table->string('alias')->unique();
				$table->string('system_name');
				$table->string('url')->unique();
				$table->unsignedInteger('template_id')->nullable();
				$table->text('fragments')->nullable();

				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages_component');
	}

}
