<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComponentPositions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('component_position')){

			Schema::create('component_position', function($table){
				$table->increments('id');

				$table->string('entity_type',50);
				$table->integer('entity_id');
				$table->string('identifier',50);

				$table->string('component_id');
				$table->text('configuration');
				$table->boolean('modified');

				$table->unique(['entity_type','entity_id','identifier']);

				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('component_position');
	}

}
