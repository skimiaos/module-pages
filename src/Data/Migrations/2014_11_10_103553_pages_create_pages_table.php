<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Skimia\Pages\Data\Models\Page;
class PagesCreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('pages_pages')){
            Schema::create('pages_pages', function($table){
                $table->increments('id');
                $table->string('name')->unique();
                $table->string('url')->unique();
                $table->text('description')->nullable();
                $table->unsignedInteger('template_id');

                $table->text('fragments')->nullable();

                $table->timestamps();
            });

            $Page =  new Page();
            $Page->template_id = 1;
            $Page->url = '/';
            $Page->name = 'Accueil';
            $Page->description = 'Page d\'accueil...';
            $Page->save();
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::drop('pages_pages');
	}

}
