<?php

namespace Skimia\Pages\Data\Models;

use Skimia\Pages\Models\AbstractFileEntity;
use Eloquent;
use Skimia\Pages\Components\ComponentContainerTrait;
use Skimia\Pages\Fragments\FragmentContainerTrait;

class Page extends Eloquent{

    use ComponentContainerTrait;
    use FragmentContainerTrait;

    protected $table = 'pages_pages';
    protected $guarded = ['id', 'created_at','updated_at'];



    public function template(){
        return $this->belongsTo('Skimia\Pages\Data\Models\Template');
    }


    public function getFile(){
        return '<html></html>';
    }

    public function getUrlAttribute($value){
        if($value =='/')
            return '/';
        else{
            return trim($value,"/");
        }

    }

    public function setUrlAttribute($url){
        if($url =='/')
            $this->attributes['url'] = $url;
        else{
            $this->attributes['url'] = trim($url,"/");
        }
    }

    public function getParentComponentContainer(){
        return $this->template;
    }

}