<?php

namespace Skimia\Pages\Data\Models;

use Skimia\Pages\Models\AbstractFileEntity;
use Eloquent;
use Skimia\Pages\Components\ComponentContainerTrait;
use Skimia\Pages\Fragments\FragmentContainerTrait;
use Theme;
class Partial extends AbstractFileEntity{

    use ComponentContainerTrait;
    use FragmentContainerTrait;
    protected $table = 'pages_partials';
    protected $guarded = ['id', 'created_at','updated_at'];

    protected $deleteFile = false;

    protected function getStorePath()
    {
        return Theme::getDefaultTheme()['path'] . '/views/pages/partials/';
    }
} 