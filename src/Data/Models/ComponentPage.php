<?php

namespace Skimia\Pages\Data\Models;

use Illuminate\Support\Collection;
use Skimia\Pages\Models\AbstractFileEntity;
use Eloquent;
use Skimia\Pages\Components\ComponentContainerTrait;
use Skimia\Pages\Fragments\FragmentContainerTrait;

class ComponentPage extends Eloquent{

    use ComponentContainerTrait;
    use FragmentContainerTrait;

    protected $table = 'pages_component';
    protected $guarded = ['id', 'created_at','updated_at'];



    public function template(){
        return $this->belongsTo('Skimia\Pages\Data\Models\Template');
    }


    public function getFile(){
        return '<html></html>';
    }

    public function getUrlAttribute($value){
        if($value =='/')
            return '/';
        else{
            return trim($value,"/");
        }

    }

    public function setUrlAttribute($url){
        if($url =='/')
            $this->attributes['url'] = $url;
        else{
            $this->attributes['url'] = trim($url,"/");
        }
    }

    public function getParentComponentContainer(){
        return $this->template;
    }

    public static function findBySysName( $sys ){
        return self::where('system_name',$sys)->get();
    }

    public static function findForceBySysName( $sys ){
        $pages = self::findBySysName($sys);

        if(count($pages) == 0){

            return new Collection([\ComponentManager::createComponentPage($sys)]);

        }
        return $pages;
    }

}