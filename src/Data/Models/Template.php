<?php

namespace Skimia\Pages\Data\Models;

use Skimia\Pages\Models\AbstractFileEntity;
use Eloquent;
use Theme;
use Skimia\Pages\Components\ComponentContainerTrait;
class Template extends AbstractFileEntity{


    use ComponentContainerTrait;

    protected $table = 'pages_templates';
    protected $guarded = ['id', 'created_at','updated_at'];


    protected $deleteFile = false;

    public function layout(){
        return $this->belongsTo('Skimia\Pages\Data\Models\Layout');
    }

    public function pages(){
        return $this->hasMany('Skimia\Pages\Data\Models\Page');
    }

    /*public function pages(){
        return $this->hasMany('Page')->getResults();
    }*/

    protected function getStorePath()
    {
        return Theme::getDefaultTheme()['path'] . '/views/pages/templates/';
    }

} 