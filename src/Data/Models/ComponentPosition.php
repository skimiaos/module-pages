<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 04/03/2015
 * Time: 17:30
 */

namespace Skimia\Pages\Data\Models;
use Skimia\Pages\Components\Component;
use Skimia\Pages\Models\AbstractFileEntity;
use File;
class ComponentPosition extends AbstractFileEntity{


    /**
     * @var Component
     */
    protected $component = null;

    protected $table = 'component_position';
    protected $guarded = ['id', 'created_at','updated_at'];

    protected $modified = false;

    public function __construct($attributes = array(),$entityType = false, $entityId = false, $identifier = false){

        if(is_array($attributes))
            parent::__construct($attributes);
        else{
            if(is_string($attributes) && is_string($entityType) && is_integer($entityId)&& is_string($identifier)){
                $this->component_id = $attributes;
                $this->entity_type = $entityType;
                $this->entity_id = $entityId;
                $this->identifier = $identifier;
            }else{
                throw new \Exception('TODO');
            }

        }

    }

    public function setComponent(Component $component){
        $this->component = $component;
    }



    protected function getStorePath()
    {
        return Theme::getDefaultTheme()['path'] . '/views/components/'.$this->entity_type.'/'.$this->entity_id.'/';
    }

    public function getPath(){
        return str_replace('.','---',$this->identifier);
    }


    public function setPath($path){
        $this->identifier = $path;
        return $this;
    }


    public function setFile($file){
        if($this->file !== $file)
            $this->modified = true;
        $this->file = $file;
    }

    public function getFile(){

        if($this->file === false){
            if($this->modified === true && File::exists($this->getFullPath()))
                $this->file = File::get($this->getFullPath());
            else
                $this->file = $this->component->getShowFile();
        }
    }

    public function getFileTemplate(){

        if($this->modified === true && File::exists($this->getFullPath()))
            return 'components/'.$this->entity_type.'/'.$this->entity_id.'/'.$this->identifier;
        else
            return $this->component->getShowTemplate();

    }

    protected $cache_configuration = false;

    public function setConfiguration(array $config){
        $this->configuration = serialize($config);
    }

    public function getConfiguration(){

        if($this->cache_configuration === false){
            if(isset($this->configuration))
                $this->cache_configuration = unserialize($this->configuration);
            else{
                $this->cache_configuration = $this->component->getDefaultConfiguration();
                $this->configuration = serialize($this->cache_configuration);
            }

        }

        return $this->cache_configuration;
    }




    public function createFile(){
        if($this->modified !== false)
            parent::createFile();
    }

    public function updateFile(){
        if($this->modified !== false)
            parent::updateFile();
    }

    public function deleteFile(){
        if($this->modified !== false)
            parent::deleteFile();
    }

    public function copyPositionTo($entity_type,$entity_id){
        self::insert([
            'entity_type'=>$entity_type,
            'entity_id'=>$entity_id,
            'identifier'=>$this->identifier,
            'configuration'=>$this->configuration,
            'component_id'=>$this->component_id

        ]);
    }
}