<?php

namespace Skimia\Pages\Data\Models;

use Skimia\Pages\Models\AbstractFileEntity;
use Skimia\Pages\Fragments\FragmentContainerTrait;
use Theme;
use Skimia\Pages\Components\ComponentContainerTrait;
class Layout extends AbstractFileEntity{


    use ComponentContainerTrait;
    use FragmentContainerTrait;

    protected $table = 'pages_layouts';
    protected $guarded = ['id', 'created_at','updated_at'];

    protected $deleteFile = false;

    public function templates(){
        return $this->hasMany('Template')->getResults();
    }

    protected function getStorePath()
    {
        return Theme::getDefaultTheme()['path'] . '/views/pages/layouts/';
    }
}