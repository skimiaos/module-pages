<?php

namespace Skimia\Pages\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Pages\Data\Models\Page;

class PagesCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Page();
    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'pages';
    }

    protected function configure(OptionsInterface $options)
    {
        $options->setTranslationContext('skimia.pages::form.pages');

        $options->Access()->simpleAccess(false);

        $options->Fields()->makeTextField('name')
            ->transAll()
            ->setDisplayOrder(1);
        $options->Fields()->makeTextField('url')
            ->transAll()
            ->setDisplayOrder(2);
        $options->Fields()->makeTextField('description')
            ->transAll()
            ->setDisplayOrder(3);
        $options->Fields()->makeRelationField('template')
            ->ManyToOneRelation()
            ->displayColumns('path')
            ->transAll()
            ->setDisplayOrder(4);

    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        $options->ActionTemplate(self::$LIST_REST_ACTION)
            ->setIcon('os-icon-docs')
            ->setTitle('Liste des pages');
        $options->ActionTemplate(self::$CREATE_REST_ACTION)
            ->setIcon('os-icon-docs')
            ->setTitle('Création dune nouvelle page');
        $options->ActionTemplate(self::$EDIT_REST_ACTION)
            ->bindBlade('skimia.pages::activities.pages-manager.forms.form')
            ->setIcon('os-icon-docs')
            ->setTitle('Edition dune page');

        $this->listConfiguration->addIdColumn();
        $this->listConfiguration->getNewColumnDefinition('name')
            ->type(ListCrudColumnConfiguration::TYPE_STRING)
            ->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('url')
            ->type(ListCrudColumnConfiguration::TYPE_STRING)
            ->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('description')
            ->type(ListCrudColumnConfiguration::TYPE_STRING)
            ->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('template')
            ->type(ListCrudColumnConfiguration::TYPE_STRING)
            ->automaticTranslatedDisplayName();

        $options->ActionFlash(self::$EDIT_REST_ACTION)
            ->setForContext('editSave','Page "%name%" editÃ©e');
        $options->ActionFlash(self::$CREATE_REST_ACTION)
            ->setForContext('createSave','Nouvelle page "%name%" crÃ©Ã©e');

    }
}