<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 24/01/2015
 * Time: 14:16
 */

namespace Skimia\Pages\Data\Forms;


use Skimia\Angular\Form\CrudForm;
use Skimia\Pages\Components\ComponentContainerFormTrait;
use Skimia\Pages\Forms\AbstractFileCrudTreeFormTrait;
use Skimia\Pages\Data\Models\Template;

class PageTemplateCrudForm extends CrudForm{

    use AbstractFileCrudTreeFormTrait;
    use ComponentContainerFormTrait;
    protected $name = 'template';
    protected $listRoute = true;
    protected $editRoute = true;
    protected $deleteRoute = true;
    protected $createRoute = true;
    protected $langKey = 'skimia.pages::form.template';

    protected $template = 'skimia.pages::activities.templating.forms.template';

    protected $hiddenFields =  ['created_at','deleted_at','updated_at','id','fragments','layout_id'];

    protected $fields = [
        'path'=>[
            'type'=>'text',
            'label'=>'NOM DE FICHIER',

        ],
        'description'=>[
            'type'=>'text',
            'label'=>'DESCRIPTION',

        ],
        'layout'=>[
            'type'=>'relation',
            'label'=>'Layout',
            'relation'=>'many_to_one',
            'show_collumn'=>'path'
        ],
        'file'=>['type'=>'text','display'=>false],
    ];

    protected function getNewEntity()
    {
        return new Template();
    }
}