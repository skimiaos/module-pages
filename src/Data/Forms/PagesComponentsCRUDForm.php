<?php

namespace Skimia\Pages\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Pages\Data\Models\ComponentPage;
use Skimia\Pages\Data\Models\Page;

class PagesComponentsCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new ComponentPage();
    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'pages_components';
    }

    protected function configure(OptionsInterface $options)
    {
        $options->setTranslationContext('skimia.pages::form.components');

        $options->Access()->simpleAccess(false);

        $options->Fields()->makeTextField('alias')
            ->transAll()
            ->setDisplayOrder(1);
        $options->Fields()->makeSelectField('system_name')
            ->setProgrammingChoices(function(){
                $components = \ComponentManager::describeComponents();


                foreach ($components as $sysName => $component) {
                    if($component['page']){
                        $choices[$sysName] = $component['name'].' url://  '.$component['page']['url'];
                    }
                }
                return $choices;
            })
            ->transAll()
            ->setDisplayOrder(2);
        $options->Fields()->makeTextField('url')
            ->transAll()
            ->setDisplayOrder(3);
        $options->Fields()->makeRelationField('template')
            ->ManyToOneRelation()
            ->displayColumns('path')
            ->transAll()
            ->setDisplayOrder(4);

    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        $options->ActionTemplate(self::$LIST_REST_ACTION)
            ->setIcon('os-icon-spread')
            ->setTitle('Liste des pages dynamiques');
        $options->ActionTemplate(self::$CREATE_REST_ACTION)
            ->setIcon('os-icon-spread')
            ->setTitle('Création dune nouvelle page dynamique');
        $options->ActionTemplate(self::$EDIT_REST_ACTION)
            ->setIcon('os-icon-spread')
            ->setTitle('Edition dune page dynamique');

        $this->listConfiguration->addIdColumn();
        $this->listConfiguration->getNewColumnDefinition('alias')
            ->type(ListCrudColumnConfiguration::TYPE_STRING)
            ->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('system_name')
            ->type(ListCrudColumnConfiguration::TYPE_STRING)
            ->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('url')
            ->type(ListCrudColumnConfiguration::TYPE_STRING)
            ->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('template')
            ->type(ListCrudColumnConfiguration::TYPE_STRING)
            ->automaticTranslatedDisplayName();

        $options->ActionFlash(self::$EDIT_REST_ACTION)
            ->setForContext('editSave','Page dynamique "%alias%" editée');
        $options->ActionFlash(self::$CREATE_REST_ACTION)
            ->setForContext('createSave','Nouvelle page dynamique "%alias%" créee');

    }
}