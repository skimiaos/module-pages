<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 24/01/2015
 * Time: 14:16
 */

namespace Skimia\Pages\Data\Forms;


use Skimia\Angular\Form\CrudForm;
use Skimia\Pages\Components\ComponentContainerFormTrait;
use Skimia\Pages\Forms\AbstractFileCrudTreeFormTrait;
use Skimia\Pages\Data\Models\Layout;

class LayoutsCrudForm extends CrudForm{

    use AbstractFileCrudTreeFormTrait;
    use ComponentContainerFormTrait;

    protected $name = 'layouts';
    protected $listRoute = true;
    protected $editRoute = true;
    protected $deleteRoute = true;
    protected $createRoute = true;
    protected $langKey = 'skimia.pages::form.layouts';
    protected $template = 'skimia.pages::activities.templating.forms.base';
    protected $hiddenFields =  ['created_at','deleted_at','updated_at','id','fragments'];

    protected $fields = [
        'file'=>['type'=>'text','display'=>false]
    ];

    protected function getNewEntity()
    {
        return new Layout();
    }


}