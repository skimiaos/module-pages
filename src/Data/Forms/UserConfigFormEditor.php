<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 02/03/2015
 * Time: 14:37
 */

namespace Skimia\Pages\Data\Forms;

use Skimia\Config\Form\UserConfigForm;

class UserConfigFormEditor extends UserConfigForm{

    protected $id = 'os.editor';
    protected $name = 'Editeur de code';
    protected $description = 'Visualiser et personnaliser la configuration de l\'éditeur de code.';
    protected $icon = 'mdi-action-polymer';
    protected $aclAction = 'templating';
    protected $saveSuccessMessage = "Mise a jour des paramètres de l\'éditeur de code. Vous pouvez réactualiser la page dés maintenant pour apprécier les changements";

    protected $configs = [
        'skimia.pages::editor.font_size' => ['label'=>'Taille de police','type'=>'select','choices'=>[
            '11'=>'11 px',
            '12'=>'12 px',
            '13'=>'13 px',
            '14'=>'14 px',
            '15'=>'15 px',
            '16'=>'16 px',
        ],'__alias'=>'font_size'],
        'skimia.pages::editor.word_wrap' => ['label'=>'Word wrap','type'=>'select','choices'=>[
            'off'=>'Off',
            '40'=>'40 Caractères',
            '80'=>'80 Caractères',
            '160'=>'160 Caractères'
        ],'__alias'=>'wwrap'],
        'skimia.pages::editor.tab_size' => ['label'=>'Taille de tabulation','type'=>'select','choices'=>[
            '2'=>'2',
            '3'=>'3',
            '4'=>'4',
            '5'=>'5',
            '6'=>'6',
            '7'=>'7',
            '8'=>'8',
        ],'__alias'=>'tab_size'],
        'skimia.pages::editor.theme' => ['label'=>'Theme','type'=>'select','choices'=>[
            'twilight'=>'Twilight',
            'ambiance'=>'Ambiance',
            'chaos'=>'Chaos',
            'chrome'=>'Chrome',
            'clouds'=>'Clouds',
            'clouds_midnight'=>'Clouds Midnight',
            'cobalt'=>'cobalt',
            'crimson_editor'=>'crimson_editor',
            'dawn'=>'dawn',
            'dreamweaver'=>'dreamweaver',
            'eclipse'=>'eclipse',
            'github'=>'github',
            'idle_fingers'=>'Idle Fingers',
            'katzenmilch'=>'Katzenmilch',
            'kr'=>'kr',
            'kuroir'=>'kuroir',
            'merbivore'=>'merbivore',
            'merbivore_soft'=>'Merbivore Soft',
            'mono_industrial'=>'Mono Industrial',
            'monokai'=>'Monokai',
            'pastel_on_dark'=>'Pastel On Dark',
            'solarized_dark'=>'Solarized Dark',
            'solarized_light'=>'Solarized Light',
            'terminal'=>'terminal',
            'textmate'=>'textmate',
            'tomorrow'=>'tomorrow',
            'tomorrow_night'=>'Tomorrow Night',
            'tomorrow_night_blue'=>'Tomorrow Night Blue',
            'tomorrow_night_bright'=>'Tomorrow Night Bright',
            'tomorrow_night_eighties'=>'Tomorrow Night Eighties',
            'vibrant_ink'=>'Vibrant Ink',
            'xcode'=>'Xcode'
        ],'__alias'=>'theme'],
        'skimia.pages::editor.show_invisibles'=>['label'=>'Afficher les caractères invisibles','type'=>'checkbox','__alias'=>'invisibles'],
        'skimia.pages::editor.highlight_active_line'=>['label'=>'Sélectionner la ligne active','type'=>'checkbox','__alias'=>'active_line'],
        'skimia.pages::editor.use_hard_tabs'=>['label'=>'Indentation par tabulation','type'=>'checkbox','__alias'=>'indent_by_tab'],
        'skimia.pages::editor.show_gutter'=>['label'=>'Afficher les numero de lignes','type'=>'checkbox','__alias'=>'gutter'],
    ];

    protected $template = 'skimia.pages::config.editor';
}