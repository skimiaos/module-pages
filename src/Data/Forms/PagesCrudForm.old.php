<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 24/01/2015
 * Time: 14:16
 */

namespace Skimia\Pages\Data\Forms;


use Skimia\Angular\Form\CrudForm;
use Skimia\Pages\Components\ComponentContainerFormTrait;
use Skimia\Pages\Data\Models\Page;


class PagesCrudForm extends CrudForm{

    //use AbstractFileCrudTreeFormTrait;
    use ComponentContainerFormTrait;

    protected $name = 'pages';
    protected $list = true;
    protected $edit = true;
    protected $create = true;
    protected $delete = true;
    protected $langKey = 'skimia.pages::form.page';

    protected $template = 'skimia.pages::activities.pages-manager.forms.form';

    protected $fields = [
        'name'=>[
            'type'=>'text',
            'label'=>'Titre'
        ],
        'url'=>[
            'type'=>'text',
            'label'=>'Adresse'
        ],
        'description'=>[
            'type'=>'text',
            'label'=>'Description'
        ],
        'template'=>[
            'type'=>'relation',
            'label'=>'Template',
            'relation'=>'many_to_one',
            'show_collumn'=>'path'
        ],
    ];
    protected $hiddenFields =  ['created_at','deleted_at','updated_at','id','template_id','fragments'];

    protected $listFields = [
        'id'=>['type'=>'text','label'=>'ID'],
        'name'=>['type'=>'text','label'=>'Titre'],
        'url'=>['type'=>'text','label'=>'Adresse'],
        'description'=>['type'=>'text','label'=>'Description'],
    ];

    protected function getNewEntity()
    {
        return new Page();
    }

    protected function afterCreateSave($entity){
        $components = $entity->template()->getResults()->getComponents();

        foreach($components as $component){
            $component->getPosition()->copyPositionTo('pages_pages',$entity->id);
        }

        return $entity;
    }

}