<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 14:59
 */
namespace Skimia\Pages\Controllers;

use Skimia\Pages\Data\Models\ComponentPage;
use Skimia\Pages\Facades\ComponentManager;
use Skimia\Pages\Data\Models\Layout;
use Skimia\Pages\Data\Models\Partial;
use Skimia\Pages\Data\Models\Template;
use Skimia\Pages\Data\Models\Page;
use Event;

class ComponentController extends \Controller{

    public function bind($class_name,$id,$componentName,$identifier = false){

        $class = $this->getEntity($class_name,$id);

        if(!ComponentManager::has($componentName)){
            throw new \Exception('demande d\'ajouter un component qui existe au moins stp');
        }
        if($identifier ===false){

            for($i = 1;$i < 500;$i++){
                if(!$class->hasComponent($componentName.'_'.$i)){
                    $identifier = $componentName.'_'.$i;
                    break;
                }
            }
            if($identifier ===false) {
                $identifier = $componentName.'_'.substr(md5(microtime()),0,6);
            }
        }

        $comp = $class->addComponent($componentName,$identifier);

        Event::fire('skimia.pages::components.bind',[
            $class,
            $comp,
            $componentName,
            $identifier
        ]);

        \AResponse::addMessage('Composant Ajouté à l\'identifiant : "'.$comp->getPosition()->identifier.'""');
        return \AResponse::r( [
            'identifier'=>$comp->getPosition()->identifier,
            'info'=>$comp->getDescriptionData(),
            'template'=>$comp->renderForm(),
            'form'=> $comp->onForm()
        ]);
    }

    public function unBind($class,$id,$identifier)
    {
        $class = $this->getEntity($class,$id);

        $class->unBindComponent($identifier);
        Event::fire('skimia.pages::components.unbind',[
            $class,
            $identifier
        ]);
        \AResponse::addMessage('Composant['.$identifier.'] Supprimé','info');
        return \AResponse::r( [
            'identifier'=>$identifier
        ]);
    }

    public function configSave($class,$id,$identifier){

        $class = $this->getEntity($class,$id);
        $component = $class->getComponent($identifier);
        if($component->isValid()){
            $component->savePosition();
            \AResponse::addMessage('Composant['.$identifier.'] Modifié');
            return \AResponse::r($component->all());
        }
        die('non valide');

    }

    public function renderTest($class,$id){
        $dir = explode('_',$class)[1];
        $class = $this->getEntity($class,$id);

        return \View::make('pages/'.$dir.'/'.$class->getPath(),['file'=>$class]);
    }


    /**
     * @param $class
     * @param $id
     * @return \Skimia\Pages\Components\ComponentContainerTrait
     */
    public function getEntity($class,$id){
        switch($class){
            case 'layout':
                return Layout::findOrFail($id);
            case 'partial':
                return Partial::findOrFail($id);
            case 'page':
                return Page::findOrFail($id);
            case 'template':
                return Template::findOrFail($id);
            case 'component':
                return ComponentPage::findOrFail($id);
        }
    }
}