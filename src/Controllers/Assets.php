<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 04/05/2015
 * Time: 15:18
 */

namespace Skimia\Pages\Controllers;


use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\File;

class Assets extends \Controller{

    public function listAssets(){

        $files = \File::allFiles(\Assets::getPrivateAssetDir().'/');

        if(!\Cache::has('skimia.pages::assets.mimes')){
            $this->makeAssetsCache($files);
        }
        $mimes = \Cache::get('skimia.pages::assets.mimes');

        $_files = [];
        foreach ($files as $file)
        {
            $name =  str_replace ( \Assets::getPrivateAssetDir(), '', (string)$file );
            $name = trim(trim($name,'/'),'\\');
            $metaFile =  ['id'=>trim(str_replace(['/','\\'],['*','*'],$name),'*'),'path'=>$name,'__TYPE'=>'FILE'];
            if(!isset($mimes[(string)$file])){
                $mimes = $this->makeAssetsCache($files);

            }

            $metaFile = array_merge($metaFile,$mimes[(string)$file]);

            $_files[] = $metaFile;


        }

        return \AResponse::r(['entities'=>$this->makeTree($_files,'path')]);
    }

    protected function makeAssetsCache($files){
        $assets = [];
        foreach ($files as $file) {
            $fileClass = new File((string)$file);

            $assets[(string)$file] = [
                'size'=>$this->formatBytes($fileClass->getSize()).'o',
                'mime'=>$fileClass->getMimeType(),
                'modified'=>date("j/n/y",$fileClass->getMTime())
            ];

        }
        \Cache::add('skimia.pages::assets.mimes',$assets,60);
        return $assets;

    }
    public function formatBytes($size, $precision = 0)
    {

    	if($size < 0)
    		return 'Inconnu';
    	elseif($size == 0)
    		return '0 ';
        $base = log($size, 1024);
        $suffixes = array('', 'k', 'M', 'G', 'T');

        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }

    public function makeTree($files,$key_path){
        $tree = [];
        foreach($files as $file){

            $path = $file[$key_path];

            $path_parts = $this->multiexplode(['\\','/'],$path);

            $i = 0;
            $len = count($path_parts);

            $current = &$tree;
            foreach($path_parts as $part){

                if(!isset($current[$part]))
                    $current[$part] = [];

                if ($i == $len - 1) {
                    $current[$part] = $file;
                }
                $i++;
                $current = &$current[$part];
            }
        }
        return $tree;
    }

    public function multiexplode ($delimiters,$string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }

    public function getAsset($asset_name){
        $file_path =  str_replace('*',DIRECTORY_SEPARATOR,$asset_name);
        $path = \Assets::getPrivateAssetDir().DIRECTORY_SEPARATOR.$file_path;

        if(!$this->is_ascii($path)){
            \AResponse::addMessage('Le fichier ne contient pas de texte modifiable','warning');
            return \AResponse::r(['ack' =>false]);
        }
        try{

            $file = \File::get($path);

        }catch(\Exception $e){
            \AResponse::addMessage('WIP : Impossible d\'afficher ce fichier');
            return \AResponse::r(['ack' =>false]);
        }

        return \AResponse::r(['ack' =>true,
            'id'=>$asset_name,
            'path' => $file_path,
            'form'=>[
                'id'=>$asset_name,
                'file' => $file,
                'path' => $file_path
            ]
        ]);



    }
    private function is_ascii($sourcefile)
    {
        if (is_file($sourcefile))
        {
            $fClass = new File($sourcefile);
            $mimes = explode('/',$fClass->getMimeType());
            switch($mimes[0]){
                case 'text':
                    return true;
                case 'inode':
                    return true;

                case 'image':
                    switch($mimes[1]){
                        case 'svg+xml':
                            return true;
                        default:
                            return false;
                    }

            }
            return true;
            $content = str_replace(array("\n", "\r", "\t", "\v", "\b"), '', file_get_contents($sourcefile));
            return ctype_print($content);

        }
        else
        {
            return false;
        }

    }

    public function setAsset($asset_name){
        $base_path = str_replace('*',DIRECTORY_SEPARATOR,$asset_name);
        $path = \Assets::getPrivateAssetDir().DIRECTORY_SEPARATOR.str_replace('*',DIRECTORY_SEPARATOR,$asset_name);
        \File::put($path,Input::get('file'));
        if($base_path != Input::get('path')){
            $new_path = \Assets::getPrivateAssetDir().DIRECTORY_SEPARATOR.Input::get('path');
            if(!\File::exists(dirname($new_path)))
                \File::makeDirectory(dirname($new_path),0700,true);
            \File::move($path,$new_path);
            $path = $new_path;
        }
        \Event::fire('os.asset.save', [null,$path]);
        \AResponse::addMessage('Asset "'.Input::get('path').'" Enregistré');
        return $this->getAsset(str_replace(['\\','/'],'*',Input::get('path')));
    }

    public function deleteAsset($asset_name){
        $path = \Assets::getPrivateAssetDir().DIRECTORY_SEPARATOR.str_replace('*',DIRECTORY_SEPARATOR,$asset_name);

        \File::delete($path);
        \AResponse::addMessage('L\'asset '.str_replace('*',DIRECTORY_SEPARATOR,$asset_name).' a bien été supprimé');
        return \AResponse::r([]);
    }

    public function createAsset(){
        $path = trim(\Input::get('path'),"\\/");
        $name = str_replace(['\\','/'],'*',$path);
        $general_path = \Assets::getPrivateAssetDir().DIRECTORY_SEPARATOR.$path;
        if(!\File::exists(dirname($general_path))){
            \File::makeDirectory(dirname($general_path),0700,true);
        }

        \File::append($general_path,'');
        return \AResponse::r(
            [
                'id'=>$name,
                'path'=>$path,
            ]
        );
        //return \File::delete($path);
    }


}