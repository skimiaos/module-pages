<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 01/04/2015
 * Time: 17:06
 */

namespace Skimia\Pages\Controllers;

use Response;
use Illuminate\Http\Response as Http;
use Skimia\Auth\Traits\Acl;
use Skimia\Pages\Facades\Pages as PagesManager;
use Skimia\Pages\Data\Models\Page;
use Skimia\Pages\Data\Models\Partial;
use Skimia\Pages\Data\Models\Layout;
use Event;

use Angular;
class Pages extends \Controller {

    use Acl;

    public function home(){
        return $this->returnPage(Page::where('url', '/')->firstOrFail());
    }

    public function page($pageUrl){
        return $this->returnPage(Page::where('url', $pageUrl)->firstOrFail());

    }

    public function componentPage($page){
        return $this->returnComponentPage($page);

    }

    protected function returnPage($page){
        $content = PagesManager::renderPage($page);

        return $this->injectLiveInContent($content);

    }

    protected function returnComponentPage($page){
        $content =  PagesManager::renderComponentPage($page);

        return $this->injectLiveInContent($content);

    }

    protected function injectLiveInContent($content){
        if($this->getAcl()->can('live_edit')){
            $front_app = Angular::get('front_angular_app');
            $content = $front_app->renderHtmlContent($content,['@'.\Assets::get('elfinder/popup.js.php','skimia.explorer'),'@'.\Assets::get('colorbox/jquery.colorbox.js','skimia.explorer')],['@'.\Assets::css('fonticons.css', 'skimia.backend'),'@'.\Assets::get('colorbox/example5/colorbox.css','skimia.explorer'),'front_live.css']);
        }

        $content = $this->injectBefore('<script type="text/javascript" src="'.route('skimia.pages::front-assets.js').'"></script>','</body>',$content);
        return $this->injectBefore('<link rel="stylesheet" href="'.route('skimia.pages::front-assets.css').'">','</head>',$content);
    }

    public function error404(){
        header("HTTP/1.0 404 Not Found");
        $page = Page::where('url', '404.html')->first();
        if(empty($page)){

            die('404 Error');
        }
        return $this->returnPage($page);
    }



    public function frontAssetsJS(){
        Event::fire('front.generate.js');

        return Response::make(\Pages::getRAWJS(), Http::HTTP_OK, ['Content-Type' => 'text/javascript']);
    }

    public function frontAssetsCSS(){
        Event::fire('front.generate.css');
        return Response::make(\Pages::getRAWCSS(), Http::HTTP_OK, ['Content-Type' => 'text/css']);
    }

    protected function injectBefore($html, $before = '</body>', $content = false){
        if(!$content){
            $content = $this->content;
        }

        $pos = strripos($content, $before);
        if (false !== $pos) {
            $content = substr($content, 0, $pos) . $html . substr($content, $pos);
        } else {
            $content = $content . $html;
        }
        return $content;
    }

    public function saveFragment(){
        $all = \Input::all();
        $e = $this->getEntity(
            $all['__table'],
            $all['__id'],
            false
        );
        $fragments = $e->getFragments();

        $identifier = $all['__identifier'];
        unset($all['__identifier']);
        unset($all['__id']);
        unset($all['__table']);

        $fragments[$identifier] = $all;
        $e->setFragments($fragments);
        $e->save();
        //dd($e);

        return \AResponse::r([]);

    }


    /**
     * @param $table
     * @param $id
     * @return \Skimia\Pages\Components\ComponentContainerTrait
     */
    public function getEntity($table,$id, $full=false){
        switch($table){
            case 'pages_layouts':
                return Layout::findOrFail($id,$full?['*']:['id','fragments']);
            case 'pages_partials':
                return Partial::findOrFail($id,$full?['*']:['id','fragments']);
            case 'pages_pages':
                return Page::findOrFail($id,$full?['*']:['id','fragments']);
        }
    }
}
