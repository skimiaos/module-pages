<?php


namespace Skimia\Pages\Listeners;

use ComponentManager;
use Pages;
use Skimia\Pages\Data\Models\ComponentPosition;

class FrontAssetsHandler
{

    public function subscribe($events)
    {
        $events->listen('front.generate.js', 'Skimia\Pages\Listeners\FrontAssetsHandler@onFrontJS');

        $events->listen('front.generate.css', 'Skimia\Pages\Listeners\FrontAssetsHandler@onFrontCSS');
    }


    public function onFrontJS()
    {
        $componentsPositions = ComponentPosition::all();

        $rawScript = '';
        $registeredComponents = [];

        foreach ($componentsPositions as $componentPos) {
            $className = ComponentManager::getComponentClass($componentPos->component_id);

            $component = new $className($componentPos);

            if(!isset($registeredComponents[$componentPos->component_id])){

                $rawScript .= ';'.$component->getStaticJS();
                $registeredComponents[$componentPos->component_id] = true;
            }

            $rawScript .= ';'.$component->getDynJS();
        }

        //dd($componentsPositions);
        Pages::addRAWJS($rawScript);
    }

    public function onFrontCSS()
    {
        $componentsPositions = ComponentPosition::all();

        $rawScript = '';
        $registeredComponents = [];

        foreach ($componentsPositions as $componentPos) {
            $className = ComponentManager::getComponentClass($componentPos->component_id);

            $component = new $className($componentPos);

            if(!isset($registeredComponents[$componentPos->component_id])){

                $rawScript .= ';'."\n".$component->getStaticCSS();
                $registeredComponents[$componentPos->component_id] = true;
            }

            $rawScript .= ';'."\n".$component->getDynCSS();
        }

        //dd($componentsPositions);
        Pages::addRAWCSS($rawScript);
    }
}