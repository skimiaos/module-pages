<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 25/01/2015
 * Time: 14:28
 */

namespace Skimia\Pages\Models;

use Eloquent;
use File;
use Theme;
abstract class AbstractFileEntity extends Eloquent {


    public $upd_files = true;
    protected abstract function getStorePath();
    protected $file = false;

    protected $deleteFile = true;
    public function getPath(){
        /*if(empty($this->path)){
            var_dump(debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT,4));
            throw new \Exception('le model '.get_class($this).' n\'est pas totalement chargé il lui manque l\'attribut path');
        }*/
        return str_replace('.','---',$this->path);
    }

    public function getFullPath(){
        return $this->getStorePath() . $this->getPath() . '.blade.php';
    }

    public function setPath($path){
        $this->path = str_replace('---','.',$path);
        return $this;
    }

    public function getFile(){
        if($this->file === false){
            if(File::exists($this->getFullPath()))
                $this->file = File::get($this->getFullPath());
            else
                $this->file ='';
        }


        return $this->file;
    }

    public function setFile($file){
        $this->file = $file;
    }

    public static function boot(){
        parent::boot();



        self::creating(function($entity){
            if($entity->upd_files)
                $entity->createFile();
        });

        self::updating(function($entity){
            if($entity->upd_files)
                $entity->updateFile();

        });
        //si aucun champ n'est deffinit saving est lancé quand même.
        self::saving(function($entity){
            if($entity->upd_files)
                $entity->updateFile();
        });

        self::deleting(function($entity){
            if($entity->upd_files)
                $entity->deleteFile();
        });
    }



    public function createFile(){

        if(!File::exists(dirname($this->getFullPath())))
            File::makeDirectory(dirname($this->getFullPath()),0777,true);

        if(!File::exists($this->getFullPath()))
            File::put($this->getFullPath(), '<!-- Vous pouvez commencer a éditer votre template -->');
    }

    public function updateFile(){

        if(!empty($this->path)) {
            $bdd_entity = $this->find($this->id);

            if ($bdd_entity) {

                if ($this->getFullPath() != $bdd_entity->getFullPath()) {
                    File::delete($bdd_entity->getFullPath());

                    if (!File::exists(dirname($this->getFullPath())))
                        File::makeDirectory(dirname($this->getFullPath()), 0777, true);
                }

                File::put($this->getFullPath(), $this->getFile());
            }
        }
    }


    public function deleteFile(){
        if($this->deleteFile)
            File::delete($this->getFullPath());
    }
}