<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 12/05/2015
 * Time: 16:35
 */
namespace Skimia\Pages\Forms;

trait AbstractFileCrudTreeFormTrait{


    protected function transformList($entities){
        $list = parent::transformList($entities);
        return $this->makeTree($list,'path');

    }

    public function makeTree($files,$key_path){
        $tree = [];
        foreach($files as $file){

            $path = $file[$key_path];

            $path_parts = $this->multiexplode(['\\','/'],$path);

            $i = 0;
            $len = count($path_parts);

            $current = &$tree;
            foreach($path_parts as $part){

                if(!isset($current[$part]))
                    $current[$part] = [];

                if ($i == $len - 1) {
                    $current[$part] = array_merge($file,['__TYPE'=>'FILE','mime'=>'text/html','info'=>$file['description']]);
                }
                $i++;
                $current = &$current[$part];
            }
        }
        return $tree;
    }

    public function multiexplode ($delimiters,$string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
}