<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/11/2014
 * Time: 09:28
 */

namespace Skimia\Pages\Facades;

use \Illuminate\Support\Facades\Facade;

class Pages extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Pages\Managers\Pages';
    }
} 