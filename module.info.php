<?php

return [
    'name'        => 'Undefined Module',
    'author'      => 'Undefined Author',
    'description' => 'Undefined Description',
    'namespace'   => 'Skimia\\Pages',
    'require'     => ['skimia.modules','skimia.blade','skimia.form','skimia.angular','skimia.auth','skimia.backend']
];