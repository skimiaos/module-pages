<?php
Route::group(array('namespace' => 'Skimia\Pages\Controllers'), function()
{



    Route::post('/fragments/save',array('as'=>'skimia.pages::fragment.save','uses'=>'Pages@saveFragment'));


    Route::get('/assets.css',[
        'as'=>'skimia.pages::front-assets.css',
        'uses'=>'Pages@frontAssetsCSS'
    ]);

    Route::get('/assets.js',[
        'as'=>'skimia.pages::front-assets.js',
        'uses'=>'Pages@frontAssetsJS'
    ]);

});



//$front_app->addDependency('angular-redactor',module_path('skimia.pages','js/angular-redactor.js'));