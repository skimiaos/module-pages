<?php

Route::group(array('namespace' => 'Skimia\Pages\Controllers'), function()
{
    /**
     * Angular Section
     */

    $app = Angular::get(OS_APPLICATION_NAME);


    \Route::get('/assets/pages/crud/tpl.html',['as'=>'skimia.pages::crud-tpl','uses'=>function(){
        return View::make('skimia.pages::directive.crud-fm')->render();
    }]);

    Route::get('/test-component/{class}/{id}/render', array('uses' => 'ComponentController@renderTest'));
    Route::get('/test-component/{class}/{id}/{identifier}/unbind', array('as'=>'skimia.pages::components.unbind','uses' => 'ComponentController@unBind'));
    Route::get('/test-component/{class}/{id}/{componentName}/{identifier?}', array('as'=>'skimia.pages::components.bind','uses' => 'ComponentController@bind'));


    Route::post('/test-component/{class}/{id}/{identifier}/save', array('as'=>'skimia.pages::components.edit', 'uses' => 'ComponentController@configSave'));


    Route::get('/pages/assets-list', array('as'=>'skimia.pages::assets.list','uses' => 'Assets@listAssets'));

    Route::get('/pages/assets-get/{name}', array('as'=>'skimia.pages::assets.get','uses' => 'Assets@getAsset'));
    Route::post('/pages/assets-set/{name}', array('as'=>'skimia.pages::assets.set','uses' => 'Assets@setAsset'));
    Route::delete('/pages/assets-delete/{name}', array('as'=>'skimia.pages::assets.delete','uses' => 'Assets@deleteAsset'));
    Route::post('/pages/assets-create', array('as'=>'skimia.pages::assets.create','uses' => 'Assets@createAsset'));
    Route::match(array('POST', 'DELETE'),'/pages/assets-upload/{simplyPath}', array('as'=>'skimia.pages::assets.post','uses' => 'Assets@uploadAsset'));

});

