<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 06/05/2015
 * Time: 19:24
 */


$app = Angular::get(OS_APPLICATION_NAME);


$pages = \Skimia\Pages\Data\Forms\PagesCRUDForm::register($app, 'pages_manager');
$pages = \Skimia\Pages\Data\Forms\PagesComponentsCRUDForm::register($app, 'pages_manager');

$app->addState('pages_manager','/pages-manager',function($params) use($pages){
    return View::make('skimia.pages::activities.pages-manager.index');
});
