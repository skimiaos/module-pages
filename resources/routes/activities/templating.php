<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 06/05/2015
 * Time: 19:24
 */


$app = Angular::get(OS_APPLICATION_NAME);


$layout = \Skimia\Pages\Data\Forms\LayoutsCrudForm::register($app, 'pages');
$partial = \Skimia\Pages\Data\Forms\PartialsCrudForm::register($app, 'pages');
$template = \Skimia\Pages\Data\Forms\PageTemplateCrudForm::register($app, 'pages');


$app->addState('templating','/templating',function($params) use($layout,$partial,$template){
    return View::make('skimia.pages::activities.templating.index',
        ['types'=>[
            'layout'=> array_merge($layout->getUrls(),['icon'=>'mdi-action-view-quilt']),
            'partial'=> array_merge($partial->getUrls(),['icon'=>'os-icon-tags-1']),
            'template'=> array_merge($template->getUrls(),['icon'=>'mdi-action-view-carousel']),
            'assets'=> [
                'list'=> route('skimia.pages::assets.list'),
                'icon'=>' os-icon-attach-5',
                'get_edit'=> route('skimia.pages::assets.get',['name'=>':id']),
                'create'=> route('skimia.pages::assets.create'),
                'delete'=> route('skimia.pages::assets.delete',['name'=>':id']),
                'edit'=> route('skimia.pages::assets.set',['name'=>':id']),
                'form'=> route('skimia.pages::assets-tpl')
            ]
        ]],$params);
});

\Route::get('/assets/pages/assets/tpl.html',['as'=>'skimia.pages::assets-tpl','uses'=>function(){
    return View::make('skimia.pages::activities.templating.forms.assets')->render();
}]);