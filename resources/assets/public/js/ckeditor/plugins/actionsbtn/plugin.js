CKEDITOR.plugins.add( 'actionsbtn', {
    init: function( editor ) {
        editor.ui.addButton('Save', {
            label: 'Enregistrer',
            toolbar: 'document'
        });
        editor.ui.addButton('Back', {
            label: 'Annuler',
            toolbar: 'document'
        });
    }
});