/// <reference path="angular.d.ts"/>
var Skimia;
(function (Skimia) {
    var Angular;
    (function (Angular) {
        var Module = (function () {
            function Module(name, requires, configFn) {
                if (requires === void 0) { requires = []; }
                if (configFn === void 0) { configFn = function () { }; }
                this.module = null;
                this.module = angular.module(name, requires, configFn);
            }
            Module.prototype.bindService = function (name, service) {
                service.$inject = service.injectables;
                var srv = service.prototype;
                this.module.service(name, srv);
            };
            Module.prototype.bindFilter = function () {
            };
            Module.prototype.bindComponent = function (component) {
                var config = new component();
                var directiveFactory = function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i - 0] = arguments[_i];
                    }
                    var isDef = angular.isDefined;
                    var directive = {};
                    if (isDef(config.restrict)) {
                        directive['restrict'] = config.restrict;
                    }
                    if (isDef(config.priority)) {
                        directive['priority'] = config.priority;
                    }
                    if (isDef(config.view)) {
                        if (isDef(config.view.templateCache)) {
                            var $templateCache = angular.element(document.body).injector().get('$templateCache');
                            directive['template'] = $templateCache.get(config.view.templateCache);
                        }
                        else if (isDef(config.view.templateUrl)) {
                            directive['templateUrl'] = config.view.templateUrl;
                        }
                        else if (isDef(config.view.template)) {
                            directive['template'] = config.view.template;
                        }
                        if (isDef(config.view.transclude)) {
                            directive['transclude'] = config.view.transclude;
                        }
                        if (isDef(config.view.replace)) {
                            directive['replace'] = config.view.replace;
                        }
                    }
                    if (isDef(config.manager)) {
                        if (config.manager.class.injectables.length > 0) {
                            directive['controller'] = config.manager.class.injectables.concat(config.manager.class);
                        }
                        else {
                            directive['controller'] = config.manager.class;
                        }
                        directive['controllerAs'] = config.manager.alias;
                        if (isDef(config.manager.isolated) && config.manager.isolated) {
                            directive['scope'] = {};
                        }
                        else {
                            directive['scope'] = true;
                        }
                        if (isDef(config.manager.bindings)) {
                            directive['bindToController'] = config.manager.bindings;
                        }
                    }
                    if (isDef(config.compile)) {
                        directive['compile'] = config.compile;
                    }
                    else if (isDef(config.link)) {
                        directive['link'] = config.link;
                    }
                    return directive;
                };
                this.module.directive(config.selector, directiveFactory);
            };
            return Module;
        })();
        Angular.Module = Module;
        var Service = (function () {
            function Service() {
                var _this = this;
                var injectables = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    injectables[_i - 0] = arguments[_i];
                }
                this.services = {};
                angular.forEach(injectables, function (injectable, index) {
                    _this.services[_this.constructor['injectables'][index]] = injectable;
                });
                this.init();
            }
            Service.prototype.init = function () {
            };
            return Service;
        })();
        Angular.Service = Service;
        var Manager = (function () {
            function Manager() {
                var _this = this;
                var injectables = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    injectables[_i - 0] = arguments[_i];
                }
                this.services = {};
                angular.forEach(injectables, function (injectable, index) {
                    _this.services[_this.constructor['injectables'][index]] = injectable;
                });
                this.init();
            }
            Manager.prototype.init = function () {
            };
            Manager.injectables = [];
            return Manager;
        })();
        Angular.Manager = Manager;
        var Component = (function () {
            function Component() {
            }
            return Component;
        })();
        Angular.Component = Component;
    })(Angular = Skimia.Angular || (Skimia.Angular = {}));
})(Skimia || (Skimia = {}));

/// <reference path="vendors/framework.ts"/>
var liveEditApp = new Skimia.Angular.Module('pages');

/// <reference path="pagesApp.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var LiveCharm = (function (_super) {
    __extends(LiveCharm, _super);
    function LiveCharm() {
        _super.apply(this, arguments);
        this.selector = 'liveCharm';
        this.restrict = 'E';
        this.manager = {
            alias: 'LiveCharm',
            class: LiveCharmManager,
            bindings: {}
        };
        this.view = {
            templateCache: 'LiveCharm.html',
            transclude: true
        };
    }
    return LiveCharm;
})(Skimia.Angular.Component);
var LiveCharmManager = (function (_super) {
    __extends(LiveCharmManager, _super);
    function LiveCharmManager() {
        _super.apply(this, arguments);
    }
    Object.defineProperty(LiveCharmManager.prototype, "Events", {
        get: function () {
            return this.events;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveCharmManager.prototype, "Activated", {
        get: function () {
            return this.activated;
        },
        set: function (activated) {
            this.activated = activated;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveCharmManager.prototype, "IsHover", {
        get: function () {
            return this.isHover;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveCharmManager.prototype, "IsOpenSections", {
        get: function () {
            return this.isOpenSections;
        },
        set: function (isOpen) {
            this.isOpenSections = isOpen;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveCharmManager.prototype, "IsOpenItems", {
        get: function () {
            return this.isOpenItems;
        },
        set: function (isOpen) {
            this.isOpenItems = isOpen;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveCharmManager.prototype, "Sections", {
        get: function () {
            var sections = [];
            this.services.liveService.SectionsRoot.forEach(function (section) {
                sections.push(section);
            });
            return sections;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveCharmManager.prototype, "SelectedSection", {
        get: function () {
            return this.selectedSection;
        },
        set: function (section) {
            this.selectedSection = section;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveCharmManager.prototype, "Items", {
        get: function () {
            var items = [];
            if (this.selectedSection) {
                this.selectedSection.Childs.forEach(function (child) {
                    items.push(child);
                });
            }
            return items;
        },
        enumerable: true,
        configurable: true
    });
    LiveCharmManager.prototype.init = function () {
        var _this = this;
        this.activated = false;
        this.isOpenSections = true;
        this.isOpenItems = false;
        this.registerEvents();
        this.services.liveService.LiveCharm = this;
        setTimeout(function () {
            _this.services.liveService.disableSections();
        }, 100);
    };
    LiveCharmManager.prototype.registerEvents = function () {
        var _this = this;
        var liveService = this.services.liveService;
        var Events = {
            toggleLiveEdit: function () {
                _this.activated = !_this.activated;
                if (_this.activated)
                    liveService.enableSections();
                else {
                    if (_this.SelectedSection) {
                        _this.SelectedSection.Events.close();
                    }
                    liveService.disableSections();
                }
            },
            hoverSection: function (name) {
                liveService.SectionsRoot.get(name).services.$element.trigger('mouseover');
            },
            leaveSection: function (name) {
                liveService.SectionsRoot.get(name).services.$element.trigger('mouseleave');
            },
            clickSection: function (name) {
                event.stopPropagation();
                liveService.closeSections();
                liveService.SectionsRoot.get(name).services.$element.trigger('mouseover');
                liveService.SectionsRoot.get(name).services.$element.trigger('click');
            },
            hoverItem: function (item) {
                item.services.$element.trigger('mouseover');
            },
            leaveItem: function (item) {
                item.services.$element.trigger('mouseleave');
            },
            clickItem: function (item) {
                item.services.$element.trigger('click');
            }
        };
        this.events = Events;
    };
    LiveCharmManager.injectables = ['$scope', '$element', 'liveService'];
    return LiveCharmManager;
})(Skimia.Angular.Manager);
liveEditApp.bindComponent(LiveCharm);
$('body').append('<live-charm></live-charm>');

/// <reference path="pagesApp.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var LiveService = (function (_super) {
    __extends(LiveService, _super);
    function LiveService() {
        _super.apply(this, arguments);
        this.sectionsRoot = new Map();
    }
    Object.defineProperty(LiveService.prototype, "SectionsRoot", {
        get: function () {
            return this.sectionsRoot;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveService.prototype, "LiveCharm", {
        set: function (livecharm) {
            this.liveCharm = livecharm;
        },
        enumerable: true,
        configurable: true
    });
    LiveService.prototype.registerSection = function (section) {
        if (!this.sectionsRoot) {
            this.sectionsRoot = new Map();
        }
        if (section.Parent == null) {
            this.sectionsRoot.set(section.Name, section);
        }
    };
    LiveService.prototype.disableSections = function () {
        this.sectionsRoot.forEach(function (section) {
            section.Disabled = true;
        });
    };
    LiveService.prototype.enableSections = function () {
        this.sectionsRoot.forEach(function (section) {
            section.Disabled = false;
        });
    };
    LiveService.prototype.closeSections = function () {
        this.sectionsRoot.forEach(function (section) {
            section.Events.close();
        });
    };
    LiveService.prototype.openCharmItems = function (section) {
        this.liveCharm.SelectedSection = section;
        this.liveCharm.IsOpenItems = true;
        this.liveCharm.IsOpenSections = false;
    };
    LiveService.prototype.openCharmSections = function () {
        this.liveCharm.IsOpenSections = true;
        this.liveCharm.IsOpenItems = false;
    };
    LiveService.prototype.refreshCharm = function () {
        var charmScope = this.liveCharm.services.$scope;
        if (charmScope.$$phase != '$apply') {
            charmScope.$apply();
        }
    };
    return LiveService;
})(Skimia.Angular.Service);
liveEditApp.module.service('liveService', LiveService);

/// <reference path="pagesApp.ts"/>
/// <reference path="LiveService.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var LiveEditable = (function (_super) {
    __extends(LiveEditable, _super);
    function LiveEditable() {
        _super.apply(this, arguments);
        this.selector = 'liveEditable';
        this.restrict = 'A';
        this.manager = {
            alias: 'LiveEditable',
            class: LiveEditableManager,
            bindings: {
                config: '=liveEditable'
            }
        };
        this.view = {
            templateCache: 'LiveEditable.html',
            transclude: true
        };
    }
    return LiveEditable;
})(Skimia.Angular.Component);
var LiveEditableManager = (function (_super) {
    __extends(LiveEditableManager, _super);
    function LiveEditableManager() {
        var _this = this;
        _super.apply(this, arguments);
        this.edit = function () {
            if (!_this.isCreatedEditor) {
                _this.createEditor();
            }
            _this.liveSection.Locked = true;
            _this.$element.addClass('_live-editing');
            $('#modal__' + _this.EditorId).removeClass('_hidden');
            _this.isOpen = true;
        };
        this.save = function () {
            var $angular_response = _this.services['$angular_response'];
            var cke = $('#cke_' + _this.EditorId);
            var newData = CKEDITOR.instances[_this.EditorId].getData();
            newData = newData.replace('&nbsp;', '');
            console.log(newData);
            var that = _this;
            $angular_response().post('fragments/save', {
                '__identifier': _this.config.save.identifier,
                '__table': _this.config.save.table,
                '__id': _this.config.save.id,
                'text': newData
            }).success(function () {
                //if(angular.isDefined(Materialize)){
                //    Materialize.toast('Texte sauvegardé !', 3000);
                //}
                that.config.data = newData;
                that.$element.find('._liveEditor-content').html(newData);
                CKEDITOR.instances[that.EditorId].setData(newData);
                that.back();
            });
        };
        this.back = function () {
            var editor = CKEDITOR.instances[_this.EditorId];
            if (editor.commands.maximize.state == 1)
                editor.execCommand('maximize');
            $('#modal__' + _this.EditorId).addClass('_hidden');
            _this.$element.removeClass('_live-editing');
            CKEDITOR.instances[_this.EditorId].setData(_this.config.data);
            setTimeout(function () {
                _this.liveSection.Locked = false;
            }, 50);
            _this.isOpen = false;
        };
    }
    Object.defineProperty(LiveEditableManager.prototype, "Events", {
        get: function () {
            return this.events;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveEditableManager.prototype, "$scope", {
        get: function () {
            return this.services['$scope'];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveEditableManager.prototype, "$element", {
        get: function () {
            return this.services['$element'];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveEditableManager.prototype, "Type", {
        get: function () {
            return this.config.type;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveEditableManager.prototype, "ModalHtml", {
        get: function () {
            return "\n            <div class=\"_livemodal\" id=\"modal__" + this.EditorId + "\">\n                <div class=\"_livemodal-box\">\n                    <header>\n                        Edition du texte " + this.Name + "\n                    </header>\n                    <main>\n                        <form>\n                            <textarea id=\"" + this.EditorId + "\">" + this.config.data + "</textarea>\n                        </form>\n                    </main>\n                    <footer>\n                        <a href=\"#\" class=\"_livemodal-cancel\">Annuler</a>\n                        <a href=\"#\" class=\"_livemodal-save\">Enregistrer</a>\n                    </footer>\n                </div>\n                <div class=\"_livemodal-overlay\"></div>\n            </div>\n        ";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveEditableManager.prototype, "Name", {
        get: function () {
            return this.config.save.identifier;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveEditableManager.prototype, "EditorId", {
        get: function () {
            return this.config.save.table.replace(/\W/g, '')
                + this.config.save.id.replace(/\W/g, '')
                + this.config.save.identifier.replace(/\W/g, '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveEditableManager.prototype, "IsHover", {
        get: function () {
            return this.isHover;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveEditableManager.prototype, "IsOpen", {
        get: function () {
            return this.isOpen;
        },
        enumerable: true,
        configurable: true
    });
    LiveEditableManager.prototype.init = function () {
        this.findLiveSection();
        this.registerEvents();
        this.registerCkeConfig();
        this.$element.find('._liveEditor-content').append(this.config.data);
    };
    LiveEditableManager.prototype.registerCkeConfig = function () {
        this.ckeConfig = new Map();
        this.ckeConfig.set('text', {
            format_tags: 'p;h1;h2;h3;pre',
            removeDialogTabs: 'image:advanced;link:advanced',
            resize_enabled: false,
            allowedContent: true,
            extraPlugins: 'skimiawidgets',
            removePlugins: 'elementspath',
            skin: 'minimalist',
            enterMode: CKEDITOR.ENTER_BR
        });
        this.ckeConfig.set('tinytext', {
            enterMode: CKEDITOR.ENTER_BR
        });
    };
    LiveEditableManager.prototype.createEditor = function () {
        var _this = this;
        $('body').append(this.ModalHtml);
        if (this.ckeConfig.has(this.config.type)) {
            CKEDITOR.replace(this.EditorId, this.ckeConfig.get(this.config.type));
        }
        else {
            CKEDITOR.replace(this.EditorId);
        }
        $('#modal__' + this.EditorId).find('._livemodal-cancel').on('click', function () {
            _this.back();
        });
        $('#modal__' + this.EditorId).find('._livemodal-save').on('click', function () {
            _this.save();
        });
        this.isCreatedEditor = true;
    };
    LiveEditableManager.prototype.findLiveSection = function () {
        if (!this.liveSection) {
            var iterator = this.services.$scope;
            var ctrl = null;
            while (null != iterator.$parent) {
                iterator = iterator.$parent;
                if (null != iterator && 'LiveSection' in iterator) {
                    ctrl = iterator.LiveSection;
                }
            }
            this.liveSection = ctrl;
            this.liveSection.Childs.set(this.Name, this);
        }
    };
    LiveEditableManager.prototype.registerEvents = function () {
        var _this = this;
        this.services.$element.on('click', function () {
            if (_this.liveSection.Opened) {
                _this.edit();
            }
        });
        this.services.$element.on('mouseover', function () {
            if (_this.liveSection.Opened) {
                _this.isHover = true;
                if (_this.services.$scope.$$phase != '$apply')
                    _this.services.$scope.$apply();
            }
        });
        this.services.$element.on('mouseleave', function () {
            _this.isHover = false;
            if (_this.services.$scope.$$phase != '$apply')
                _this.services.$scope.$apply();
        });
    };
    LiveEditableManager.injectables = ['$scope', '$element', '$timeout', '$angular_response'];
    return LiveEditableManager;
})(Skimia.Angular.Manager);
liveEditApp.bindComponent(LiveEditable);

/// <reference path="pagesApp.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var LiveImage = (function (_super) {
    __extends(LiveImage, _super);
    function LiveImage() {
        _super.apply(this, arguments);
        this.selector = 'liveImage';
        this.restrict = 'A';
        this.manager = {
            alias: 'LiveImage',
            class: LiveImageManager,
            bindings: {
                config: '=liveImage',
                src: '@ngSrc',
                description: '@alt'
            }
        };
    }
    return LiveImage;
})(Skimia.Angular.Component);
var LiveImageManager = (function (_super) {
    __extends(LiveImageManager, _super);
    function LiveImageManager() {
        var _this = this;
        _super.apply(this, arguments);
        this.edit = function () {
            Browse(_this.services['$element'], $('#modal__' + _this.EditorId));
        };
        this.save = function () {
            var element = _this.services['$element'];
            var $scope = _this.services['$scope'];
            var $angular_response = _this.services['$angular_response'];
            var that = _this;
            $angular_response().post('fragments/save', {
                '__identifier': _this.config.identifier,
                '__table': _this.config.table,
                '__id': _this.config.id,
                'src': _this.ModalSelector.find('.fileinput').val(),
                'description': _this.ModalSelector.find('input[name="description"]').val()
            }).success(function () {
                //if(angular.isDefined(Materialize)){
                //    Materialize.toast('Image sauvegardée !', 3000);
                //}
                element.attr('src', that.ModalSelector.find('.fileinput').val());
                that.src = element.attr('src');
                that.description = that.ModalSelector.find('input[name="description"]').val();
                that.back();
            });
        };
        this.back = function () {
            var element = $(_this.services['$element']);
            _this.ModalSelector.find('.fileinput').val(_this.src);
            _this.ModalSelector.find('input[name="description"]').val(_this.description);
            _this.ModalSelector.addClass('_hidden');
        };
    }
    Object.defineProperty(LiveImageManager.prototype, "Type", {
        get: function () {
            return 'image';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveImageManager.prototype, "Name", {
        get: function () {
            return this.config.identifier;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveImageManager.prototype, "EditorId", {
        get: function () {
            return this.config.table.replace(/\W/g, '')
                + this.config.id.replace(/\W/g, '')
                + this.config.identifier.replace(/\W/g, '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveImageManager.prototype, "ModalHtml", {
        get: function () {
            return "\n            <div class=\"_livemodal\" id=\"modal__" + this.EditorId + "\">\n                <div class=\"_livemodal-box\">\n                    <header>\n                        Edition de l'image " + this.Name + "\n                    </header>\n                    <main>\n                        <form class=\"_livemodal-imgform\">\n                            <div class=\"_livemodal-image\">\n                                <a href=\"#\" class=\"\">Choisir une image</a>\n                                <img src=\"http://placehold.it/1920x1080\" alt=\"\">\n                            </div>\n                            <input type=\"text\" class=\"fileinput\">\n                            <label>Description de la photo (Ceci aide au r\u00E9f\u00E9rencement)</label>\n                            <input type=\"text\" name=\"description\">\n                        </form>\n                    </main>\n                    <footer>\n                        <a href=\"#\" class=\"_livemodal-cancel\">Annuler</a>\n                        <a href=\"#\" class=\"_livemodal-save\">Enregistrer</a>\n                    </footer>\n                </div>\n                <div class=\"_livemodal-overlay\"></div>\n            </div>\n        ";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveImageManager.prototype, "ModalSelector", {
        get: function () {
            return $('#modal__' + this.EditorId);
        },
        enumerable: true,
        configurable: true
    });
    LiveImageManager.prototype.init = function () {
        this.findLiveSection();
        this.registerEvents();
    };
    LiveImageManager.prototype.createModal = function () {
        var _this = this;
        $('body').append(this.ModalHtml);
        var imgSrc = this.services['$element'].attr('ng-src');
        this.ModalSelector.find('._livemodal-image img').attr('src', imgSrc);
        this.ModalSelector.find('.fileinput').val(imgSrc);
        this.ModalSelector.find('input[name="description"]').val(this.description);
        this.ModalSelector.find('._livemodal-cancel').on('click', function (event) {
            event.stopPropagation();
            _this.back();
        });
        this.ModalSelector.find('._livemodal-save').on('click', function (event) {
            event.stopPropagation();
            _this.save();
        });
        this.ModalSelector.find('._livemodal-image a').on('click', function (event) {
            event.stopPropagation();
            _this.edit();
        });
        this.ModalSelector.find('.fileinput').on('input', function () {
            _this.ModalSelector.find('._livemodal-image img')
                .attr('src', _this.ModalSelector.find('.fileinput').val());
        });
    };
    LiveImageManager.prototype.findLiveSection = function () {
        if (!this.liveSection) {
            var iterator = this.services.$scope;
            var ctrl = null;
            while (null != iterator.$parent) {
                iterator = iterator.$parent;
                if (null != iterator && 'LiveSection' in iterator) {
                    ctrl = iterator.LiveSection;
                }
            }
            this.liveSection = ctrl;
            this.liveSection.Childs.set(this.Name, this);
        }
    };
    LiveImageManager.prototype.registerEvents = function () {
        var _this = this;
        var elem = this.services['$element'];
        elem.on('click', function () {
            if (_this.liveSection.Opened) {
                if (!_this.ModalSelector.length) {
                    _this.createModal();
                }
                else {
                    _this.ModalSelector.removeClass('_hidden');
                }
            }
        });
    };
    LiveImageManager.injectables = ['$scope', '$element', '$timeout', '$angular_response'];
    return LiveImageManager;
})(Skimia.Angular.Manager);
liveEditApp.bindComponent(LiveImage);

/// <reference path="pagesApp.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var LiveLink = (function (_super) {
    __extends(LiveLink, _super);
    function LiveLink() {
        _super.apply(this, arguments);
        this.selector = 'liveLink';
        this.restrict = 'A';
        this.manager = {
            alias: 'LiveLink',
            class: LiveLinkManager,
            bindings: {
                config: '=liveLink',
                text: '@',
                href: '@',
                title: '@'
            }
        };
    }
    return LiveLink;
})(Skimia.Angular.Component);
var LiveLinkManager = (function (_super) {
    __extends(LiveLinkManager, _super);
    function LiveLinkManager() {
        var _this = this;
        _super.apply(this, arguments);
        this.edit = function () {
        };
        this.save = function () {
            var element = _this.services['$element'];
            var $scope = _this.services['$scope'];
            var $angular_response = _this.services['$angular_response'];
            var that = _this;
            $angular_response().post('fragments/save', {
                '__identifier': _this.config.identifier,
                '__table': _this.config.table,
                '__id': _this.config.id,
                'text': _this.ModalSelector.find('input[name="text"]').val(),
                'href': _this.ModalSelector.find('input[name="href"]').val(),
                'title': _this.ModalSelector.find('input[name="title"]').val()
            }).success(function () {
                //if(angular.isDefined(Materialize)){
                //    Materialize.toast('Lien sauvegardé !', 3000);
                //}
                that.text = that.ModalSelector.find('input[name="text"]').val();
                that.href = that.ModalSelector.find('input[name="href"]').val();
                that.title = that.ModalSelector.find('input[name="title"]').val();
                that.$element.text(that.text);
                that.$element.attr('href', that.href);
                that.$element.attr('title', that.title);
                that.back();
            });
        };
        this.back = function () {
            var element = $(_this.services['$element']);
            _this.ModalSelector.find('input[name="text"]').val(_this.text);
            _this.ModalSelector.find('input[name="href"]').val(_this.href);
            _this.ModalSelector.find('input[name="title"]').val(_this.title);
            _this.ModalSelector.addClass('_hidden');
        };
    }
    Object.defineProperty(LiveLinkManager.prototype, "$element", {
        get: function () {
            return this.services['$element'];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveLinkManager.prototype, "Type", {
        get: function () {
            return 'link';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveLinkManager.prototype, "Name", {
        get: function () {
            return this.config.identifier;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveLinkManager.prototype, "EditorId", {
        get: function () {
            return this.config.table.replace(/\W/g, '')
                + this.config.id.replace(/\W/g, '')
                + this.config.identifier.replace(/\W/g, '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveLinkManager.prototype, "ModalHtml", {
        get: function () {
            return "\n            <div class=\"_livemodal\" id=\"modal__" + this.EditorId + "\">\n                <div class=\"_livemodal-box\">\n                    <header>\n                        Edition de l'image " + this.Name + "\n                    </header>\n                    <main>\n                        <form>\n                            <label>Texte</label>\n                            <input type=\"text\" name=\"text\">\n\n                            <label>Url</label>\n                            <input type=\"text\" name=\"href\">\n\n                            <label>Titre (Aide au r\u00E9f\u00E9rencement)</label>\n                            <input type=\"text\" name=\"title\">\n                        </form>\n                    </main>\n                    <footer>\n                        <a href=\"#\" class=\"_livemodal-cancel\">Annuler</a>\n                        <a href=\"#\" class=\"_livemodal-save\">Enregistrer</a>\n                    </footer>\n                </div>\n                <div class=\"_livemodal-overlay\"></div>\n            </div>\n        ";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveLinkManager.prototype, "ModalSelector", {
        get: function () {
            return $('#modal__' + this.EditorId);
        },
        enumerable: true,
        configurable: true
    });
    LiveLinkManager.prototype.init = function () {
        this.findLiveSection();
        this.registerEvents();
        this.$element.text(this.text);
        this.$element.attr('href', this.href);
        this.$element.attr('title', this.title);
    };
    LiveLinkManager.prototype.createModal = function () {
        var _this = this;
        $('body').append(this.ModalHtml);
        this.ModalSelector.find('input[name="text"]').val(this.text);
        this.ModalSelector.find('input[name="href"]').val(this.href);
        this.ModalSelector.find('input[name="title"]').val(this.title);
        this.ModalSelector.find('._livemodal-cancel').on('click', function (event) {
            event.stopPropagation();
            _this.back();
        });
        this.ModalSelector.find('._livemodal-save').on('click', function (event) {
            event.stopPropagation();
            _this.save();
        });
    };
    LiveLinkManager.prototype.findLiveSection = function () {
        if (!this.liveSection) {
            var iterator = this.services.$scope;
            var ctrl = null;
            while (null != iterator.$parent) {
                iterator = iterator.$parent;
                if (null != iterator && 'LiveSection' in iterator) {
                    ctrl = iterator.LiveSection;
                }
            }
            this.liveSection = ctrl;
            this.liveSection.Childs.set(this.Name, this);
        }
    };
    LiveLinkManager.prototype.registerEvents = function () {
        var _this = this;
        this.$element.on('click', function (event) {
            if (_this.liveSection.Opened) {
                event.preventDefault();
                if (!_this.ModalSelector.length) {
                    _this.createModal();
                }
                else {
                    _this.ModalSelector.removeClass('_hidden');
                }
            }
        });
        this.$element.on('mouseover', function () {
            if (_this.liveSection.Opened) {
                _this.$element.addClass('_hover');
            }
        });
        this.$element.on('mouseleave', function () {
            if (_this.liveSection.Opened) {
                _this.$element.removeClass('_hover');
            }
        });
    };
    LiveLinkManager.injectables = ['$scope', '$element', '$timeout', '$angular_response'];
    return LiveLinkManager;
})(Skimia.Angular.Manager);
liveEditApp.bindComponent(LiveLink);

/// <reference path="pagesApp.ts"/>
/// <reference path="LiveService.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var LiveSection = (function (_super) {
    __extends(LiveSection, _super);
    function LiveSection() {
        _super.apply(this, arguments);
        this.selector = 'liveSection';
        this.restrict = 'A';
        this.manager = {
            alias: 'LiveSection',
            class: LiveSectionManager,
            bindings: {
                name: '@liveSection'
            }
        };
        this.view = {
            templateCache: 'LiveSection.html',
            transclude: true
        };
    }
    return LiveSection;
})(Skimia.Angular.Component);
var LiveSectionManager = (function (_super) {
    __extends(LiveSectionManager, _super);
    function LiveSectionManager() {
        _super.apply(this, arguments);
    }
    Object.defineProperty(LiveSectionManager.prototype, "Events", {
        get: function () {
            return this.events;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveSectionManager.prototype, "Name", {
        get: function () {
            return this.name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveSectionManager.prototype, "Parent", {
        get: function () {
            return this.parent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveSectionManager.prototype, "Childs", {
        get: function () {
            return this.childs;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveSectionManager.prototype, "ChildsArray", {
        get: function () {
            var array = new Array();
            this.childs.forEach(function (child) {
                array.push(child);
            });
            return array;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveSectionManager.prototype, "Hover", {
        get: function () {
            return this.isHover;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveSectionManager.prototype, "Opened", {
        get: function () {
            return this.isOpen;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveSectionManager.prototype, "Disabled", {
        get: function () {
            return this.isDisabled;
        },
        set: function (disabled) {
            this.isDisabled = disabled;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LiveSectionManager.prototype, "Locked", {
        get: function () {
            return this.isLocked;
        },
        set: function (locked) {
            this.isLocked = locked;
        },
        enumerable: true,
        configurable: true
    });
    LiveSectionManager.prototype.init = function () {
        this.isOpen = false;
        this.isDisabled = true;
        this.isLocked = false;
        this.isHover = false;
        this.parent = null;
        this.findParent();
        this.childs = new Map();
        this.registerEvents();
        this.registerStyles();
        this.services.liveService.registerSection(this);
    };
    LiveSectionManager.prototype.edit = function () {
        this.services.liveService.closeSections();
        this.services.liveService.disableSections();
        this.isDisabled = false;
        this.isOpen = true;
        var live = this.services.liveService;
        live.openCharmItems(this);
        live.refreshCharm();
    };
    LiveSectionManager.prototype.close = function () {
        this.isOpen = false;
        this.services.liveService.enableSections();
        this.services.liveService.openCharmSections();
    };
    LiveSectionManager.prototype.registerStyles = function () {
        if (this.services.$element.css("position") == "static") {
            this.services.$element.css("position", "relative");
            console.warn('la live section "' + this.name + '" doit avoir l\'attribut css position défini pour fonctionner correctement elle a été mise à la volée', this.services.$element);
        }
    };
    LiveSectionManager.prototype.registerEvents = function () {
        var _this = this;
        var Events = {
            hover: function () {
                if (!_this.isOpen && !_this.isDisabled) {
                    _this.services.$element.addClass('_hover');
                    _this.isHover = true;
                    _this.services.liveService.refreshCharm();
                    event.stopPropagation();
                }
            },
            leave: function () {
                event.stopPropagation();
                if (!_this.isOpen && !_this.isDisabled) {
                    _this.services.$element.removeClass('_hover');
                    _this.isHover = false;
                    _this.services.liveService.refreshCharm();
                }
            },
            click: function () {
                if (!_this.isDisabled) {
                    _this.edit();
                    var element = _this.services.$element;
                    element.addClass('_editing _hover');
                    if (element.offset().top > 50) {
                        element.find('._live-element').addClass('_top');
                    }
                    else {
                        element.find('._live-element').removeClass('_top');
                    }
                }
            },
            clickout: function ($event) {
                if ($event === void 0) { $event = null; }
                event.stopPropagation();
                if (_this.isOpen && !_this.isLocked) {
                    _this.events.close();
                }
            },
            close: function () {
                if (!_this.isLocked) {
                    event.stopPropagation();
                    _this.close();
                    _this.isHover = false;
                    _this.services.liveService.refreshCharm();
                    var element = _this.services.$element;
                    element.removeClass('_editing _hover');
                    element.find('._live-element').removeClass('_top');
                }
            }
        };
        this.events = Events;
        this.services.$element.on('mouseover', Events.hover);
        this.services.$element.on('mouseleave', Events.leave);
        this.services.$element.on('clickoutside', Events.clickout);
        this.services.$element.on('click', Events.click);
    };
    LiveSectionManager.prototype.findParent = function () {
        var iterator = this.services.$scope;
        while (null != iterator.$parent) {
            iterator = iterator.$parent;
            if (null != iterator && 'LiveSection' in iterator) {
                this.parent = iterator.LiveSection;
            }
        }
    };
    LiveSectionManager.injectables = ['$scope', '$element', 'liveService'];
    return LiveSectionManager;
})(Skimia.Angular.Manager);
liveEditApp.bindComponent(LiveSection);
