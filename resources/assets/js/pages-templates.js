angular.module("pages").run(['$templateCache', function(a) { a.put('LiveSection.html', '<div ng-transclude></div>\n' +
    '<div class="_live-section-label _live-element">{{LiveSection.name}}</div>\n' +
    '<div class="_live-section-close _live-element"\n' +
    '        ng-click="LiveSection.Events.close()">\n' +
    '    <i class="os-icon-cancel-3"></i>\n' +
    '</div>');
	a.put('LiveEditable.html', '<div class="_liveEditor-content"\n' +
    '        ng-class="{\'_hover\':LiveEditable.IsHover}">\n' +
    '\n' +
    '</div>\n' +
    '<div class="_liveEditor-label"\n' +
    '        ng-class="{\'_show\':LiveEditable.IsHover}">\n' +
    '    {{ LiveEditable.Name }}\n' +
    '</div>\n' +
    '');
	a.put('LiveCharm.html', '<div class="_livecharm-activate"\n' +
    '        ng-class="{\'_disabled\':!LiveCharm.Activated}"\n' +
    '        ng-click="LiveCharm.Events.toggleLiveEdit()">\n' +
    '    <i class="os-icon-pencil-alt-1"></i>\n' +
    '</div>\n' +
    '<ul class="_livecharm-list"\n' +
    '    ng-class="{\'_show\':(LiveCharm.IsOpenSections && LiveCharm.Activated)}">\n' +
    '    <li class="_livecharm-title">Liste des sections</li>\n' +
    '    <li class="_livecharm-item"\n' +
    '        ng-repeat="section in LiveCharm.Sections"\n' +
    '        ng-mouseover="LiveCharm.Events.hoverSection(section.Name)"\n' +
    '        ng-mouseleave="LiveCharm.Events.leaveSection(section.Name)"\n' +
    '        ng-click="LiveCharm.Events.clickSection(section.Name, $event)"\n' +
    '        ng-class="{\'_hover\':section.Hover||section.Opened}">\n' +
    '        <i class="os-icon-doc-landscape"></i>\n' +
    '        <span>{{ section.Name }}</span>\n' +
    '    </li>\n' +
    '</ul>\n' +
    '<ul class="_livecharm-items"\n' +
    '    ng-class="{\'_show\':LiveCharm.IsOpenItems}">\n' +
    '    <li class="_livecharm-title">{{LiveCharm.SelectedSection.Name}}</li>\n' +
    '    <li class="_livecharm-item"\n' +
    '        ng-repeat="item in LiveCharm.Items"\n' +
    '        ng-mouseover="LiveCharm.Events.hoverItem(item)"\n' +
    '        ng-mouseleave="LiveCharm.Events.leaveItem(item)"\n' +
    '        ng-click="LiveCharm.Events.clickItem(item)"\n' +
    '        ng-class="{\'_hover\':item.IsHover}">\n' +
    '        <i class="os-icon-doc-text" ng-show="item.Type == \'text\' || item.Type == \'tinytext\'"></i>\n' +
    '        <i class="os-icon-file-image" ng-show="item.Type == \'image\'"></i>\n' +
    '        <i class="os-icon-link-2" ng-show="item.Type == \'link\'"></i>\n' +
    '        <span>{{ item.Name }}</span>\n' +
    '    </li>\n' +
    '</ul>\n' +
    '');
	 }]);