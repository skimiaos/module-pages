<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 02/03/2015
 * Time: 14:56
 */
return [
    'font_size'=>'13',
    'word_wrap'=>'fluid',
    'code_folding'=>'markbegin',
    'tab_size'=>'4',
    'theme'=>'monokai',
    'show_invisibles'=>'no',
    'highlight_active_line'=>'yes',
    'use_hard_tabs'=>'yes',
    'show_gutter'=> 'yes'
];