<?php

return [
    'layouts'=>[
        'path'=>['label'=>'NOM DE FICHIER'],
        'description'=>['label'=>'DESCRIPTION'],
    ],
    'partials'=>[
        'path'=>['label'=>'NOM DE FICHIER'],
        'description'=>['label'=>'DESCRIPTION'],
    ],
    'template'=>[
        'path'=>['label'=>'NOM DE FICHIER'],
        'description'=>['label'=>'DESCRIPTION'],
        'layout_id'=>['label'=>'Layout '],
    ],
    'page'=>[
        'id'=>['label'=>'Id'],
        'name'=>['label'=>'Titre'],
        'url'=>['label'=>'Url'],
        'description'=>['label'=>'Description'],
        'updated_at'=>['label'=>''],
        'created_at'=>['label'=>''],
        'template_id'=>['label'=>'']
    ],
    'pages'=>[
        'list'=>[
            'id'=>['label'=>'Id'],
            'name'=>['label'=>'Titre'],
            'url'=>['label'=>'Url'],
            'description'=>['label'=>'Description'],
            'template'=>['label'=>'Template']
        ],
        'fields'=>[
            'id'=>['label'=>'Id'],
            'name'=>['label'=>'Titre'],
            'url'=>['label'=>'Url'],
            'description'=>['label'=>'Description'],
            'template'=>['label'=>'Template']
        ]
    ],
    'components'=>[
        'list'=>[
            'id'=>['label'=>'Id'],
            'alias'=>['label'=>'Identifiant'],
            'url'=>['label'=>'Url'],
            'system_name'=>['label'=>'Nom Système du Component'],
            'template'=>['label'=>'Template']
        ],
        'fields'=>[
            'id'=>['label'=>'Id'],
            'alias'=>['label'=>'Identifiant'],
            'url'=>['label'=>'Url'],
            'system_name'=>['label'=>'Nom Système du Component'],
            'template'=>['label'=>'Template']
        ]
    ]
];
