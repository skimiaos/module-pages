/// <reference path="pagesApp.ts"/>

class LiveCharm extends Skimia.Angular.Component{
    selector = 'liveCharm';
    restrict = 'E';
    manager = {
        alias: 'LiveCharm',
        class: LiveCharmManager,
        bindings: {
            //name: '@liveSection'
        }
    };
    view = {
        templateCache: 'LiveCharm.html',
        transclude: true
    }
}
class LiveCharmManager extends Skimia.Angular.Manager{

    static injectables = ['$scope', '$element', 'liveService'];

    public services:{
        $scope:ng.IScope;
        $element:JQuery;
        liveService:LiveService;
    }

    protected events;
    get Events() {
        return this.events;
    }

    protected activated:boolean;
    get Activated():boolean{
        return this.activated;
    }
    set Activated(activated:boolean){
        this.activated = activated;
    }

    protected isHover:boolean;
    get IsHover():boolean {
        return this.isHover;
    }

    protected isOpenSections:boolean;
    get IsOpenSections():boolean {
        return this.isOpenSections;
    }
    set IsOpenSections(isOpen:boolean) {
        this.isOpenSections = isOpen;
    }

    protected isOpenItems:boolean;
    get IsOpenItems():boolean {
        return this.isOpenItems;
    }
    set IsOpenItems(isOpen:boolean) {
        this.isOpenItems = isOpen;
    }

    get Sections():LiveSectionManager[]{
        var sections = [];

        this.services.liveService.SectionsRoot.forEach((section) => {
            sections.push(section);
        });

        return sections;
    }

    protected selectedSection:LiveSectionManager;
    get SelectedSection():LiveSectionManager {
        return this.selectedSection;
    }
    set SelectedSection(section:LiveSectionManager) {
        this.selectedSection = section;
    }

    get Items() {
        var items = [];

        if(this.selectedSection){
            this.selectedSection.Childs.forEach((child) => {
                items.push(child);
            });
        }

        return items;
    }


    public init(){
        this.activated = false;
        this.isOpenSections = true;
        this.isOpenItems = false;

        this.registerEvents();
        this.services.liveService.LiveCharm = this;

        setTimeout(() => {
            this.services.liveService.disableSections();
        }, 100);
    }

    public registerEvents() {
        var liveService = this.services.liveService;

        var Events = {
            toggleLiveEdit: () => {
                this.activated = !this.activated;

                if(this.activated)
                    liveService.enableSections();
                else{
                    if(this.SelectedSection) {
                        this.SelectedSection.Events.close();
                    }
                    liveService.disableSections();
                }
            },
            hoverSection: (name:string) => {
                liveService.SectionsRoot.get(name).services.$element.trigger('mouseover');
            },
            leaveSection: (name:string) => {
                liveService.SectionsRoot.get(name).services.$element.trigger('mouseleave');
            },
            clickSection: (name:string) => {
                event.stopPropagation();
                liveService.closeSections();
                liveService.SectionsRoot.get(name).services.$element.trigger('mouseover');
                liveService.SectionsRoot.get(name).services.$element.trigger('click');
            },
            hoverItem: (item) => {
                item.services.$element.trigger('mouseover');
            },
            leaveItem: (item) => {
                item.services.$element.trigger('mouseleave');
            },
            clickItem: (item) => {
                item.services.$element.trigger('click');
            }
        }

        this.events = Events;
    }
}

liveEditApp.bindComponent(LiveCharm);

$('body').append('<live-charm></live-charm>');