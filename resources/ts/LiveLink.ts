/// <reference path="pagesApp.ts"/>

/**
 * LiveEditable
 */
class LiveLink extends Skimia.Angular.Component{
    selector = 'liveLink';
    restrict = 'A';
    manager = {
        alias: 'LiveLink',
        class: LiveLinkManager,
        bindings: {
            config: '=liveLink',
            text: '@',
            href: '@',
            title: '@'
        }
    };
}
class LiveLinkManager extends Skimia.Angular.Manager{
    static injectables = ['$scope', '$element', '$timeout', '$angular_response'];

    get $element():JQuery {
        return this.services['$element'];
    }

    config:{
        table:string;
        id:string;
        identifier:string;
    };

    text:string;
    href:string;
    title:string;

    get Type():string {
        return 'link';
    }

    get Name():string {
        return this.config.identifier;
    }

    get EditorId():string {
        return this.config.table.replace(/\W/g, '')
            + this.config.id.replace(/\W/g, '')
            + this.config.identifier.replace(/\W/g, '');
    }
    get ModalHtml():string {
        return `
            <div class="_livemodal" id="modal__` + this.EditorId + `">
                <div class="_livemodal-box">
                    <header>
                        Edition de l'image ` + this.Name + `
                    </header>
                    <main>
                        <form>
                            <label>Texte</label>
                            <input type="text" name="text">

                            <label>Url</label>
                            <input type="text" name="href">

                            <label>Titre (Aide au référencement)</label>
                            <input type="text" name="title">
                        </form>
                    </main>
                    <footer>
                        <a href="#" class="_livemodal-cancel">Annuler</a>
                        <a href="#" class="_livemodal-save">Enregistrer</a>
                    </footer>
                </div>
                <div class="_livemodal-overlay"></div>
            </div>
        `;
    }
    get ModalSelector():JQuery {
        return $('#modal__' + this.EditorId);
    }

    protected liveSection:LiveSectionManager;

    /**
     *
     */
    public init(){
        this.findLiveSection();
        this.registerEvents();

        //Copy text to html content
        this.$element.text(this.text);
        this.$element.attr('href', this.href);
        this.$element.attr('title', this.title);
    }

    public edit = () => {

    }

    public save = () => {
        var element = this.services['$element'];
        var $scope = this.services['$scope'];
        var $angular_response = this.services['$angular_response'];
        var that = this;

        $angular_response().post('fragments/save',{
            '__identifier': this.config.identifier,
            '__table': this.config.table,
            '__id': this.config.id,
            'text': this.ModalSelector.find('input[name="text"]').val(),
            'href': this.ModalSelector.find('input[name="href"]').val(),
            'title': this.ModalSelector.find('input[name="title"]').val()
        }).success(function(){
            //if(angular.isDefined(Materialize)){
            //    Materialize.toast('Lien sauvegardé !', 3000);
            //}

            that.text = that.ModalSelector.find('input[name="text"]').val();
            that.href = that.ModalSelector.find('input[name="href"]').val();
            that.title = that.ModalSelector.find('input[name="title"]').val();

            that.$element.text(that.text);
            that.$element.attr('href', that.href);
            that.$element.attr('title', that.title);

            that.back();
        });
    }

    public back = () => {
        var element = $(this.services['$element']);
        //element.attr('src', this.src);
        this.ModalSelector.find('input[name="text"]').val(this.text);
        this.ModalSelector.find('input[name="href"]').val(this.href);
        this.ModalSelector.find('input[name="title"]').val(this.title);
        this.ModalSelector.addClass('_hidden');
    }

    protected createModal() {
        $('body').append(this.ModalHtml);

        this.ModalSelector.find('input[name="text"]').val(this.text);
        this.ModalSelector.find('input[name="href"]').val(this.href);
        this.ModalSelector.find('input[name="title"]').val(this.title);

        this.ModalSelector.find('._livemodal-cancel').on('click', (event) => {
            event.stopPropagation();
            this.back();
        });

        this.ModalSelector.find('._livemodal-save').on('click', (event) => {
            event.stopPropagation();
            this.save();
        });
    }

    protected findLiveSection():void{
        if(!this.liveSection){
            var iterator = this.services.$scope;
            var ctrl = null

            while(null != iterator.$parent){
                iterator = iterator.$parent;

                if(null != iterator && 'LiveSection' in iterator){
                    ctrl = iterator.LiveSection;
                }
            }

            this.liveSection = ctrl;
            this.liveSection.Childs.set(this.Name, this);
        }
    }

    protected registerEvents() {
        this.$element.on('click', (event) => {
            if(this.liveSection.Opened) {
                event.preventDefault();

                if (!this.ModalSelector.length) {
                    this.createModal();
                }
                else {
                    this.ModalSelector.removeClass('_hidden');
                }
            }
        });

        this.$element.on('mouseover', () => {
            if(this.liveSection.Opened){
                this.$element.addClass('_hover');
            }
        });

        this.$element.on('mouseleave', () => {
            if(this.liveSection.Opened){
                this.$element.removeClass('_hover');
            }
        });
    }
}
liveEditApp.bindComponent(LiveLink);