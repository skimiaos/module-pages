/// <reference path="pagesApp.ts"/>
/// <reference path="LiveService.ts"/>

declare var Materialize:{toast:(string,int) => void;}

/**
 * LiveEditable
 */
class LiveEditable extends Skimia.Angular.Component{
    selector = 'liveEditable';
    restrict = 'A';
    manager = {
        alias: 'LiveEditable',
        class: LiveEditableManager,
        bindings: {
            config: '=liveEditable'
        }
    };
    view = {
        templateCache: 'LiveEditable.html',
        transclude: true
    };
}
class LiveEditableManager extends Skimia.Angular.Manager{
    static injectables = ['$scope', '$element', '$timeout', '$angular_response'];

    protected liveSection:LiveSectionManager;
    protected events;
    get Events() {
        return this.events;
    }

    get $scope():ng.IScope {
        return this.services['$scope'];
    }
    get $element():JQuery {
        return this.services['$element'];
    }

    protected config:{
        type:string;
        data:string;
        save:{
            table:string;
            id:string;
            identifier:string;
        }
    };
    get Type():string {
        return this.config.type;
    }
    get ModalHtml():string {
        return `
            <div class="_livemodal" id="modal__` + this.EditorId + `">
                <div class="_livemodal-box">
                    <header>
                        Edition du texte ` + this.Name + `
                    </header>
                    <main>
                        <form>
                            <textarea id="` + this.EditorId + `">` + this.config.data + `</textarea>
                        </form>
                    </main>
                    <footer>
                        <a href="#" class="_livemodal-cancel">Annuler</a>
                        <a href="#" class="_livemodal-save">Enregistrer</a>
                    </footer>
                </div>
                <div class="_livemodal-overlay"></div>
            </div>
        `;
    }
    get Name():string {
        return this.config.save.identifier;
    }
    get EditorId():string {
        return this.config.save.table.replace(/\W/g, '')
            + this.config.save.id.replace(/\W/g, '')
            + this.config.save.identifier.replace(/\W/g, '');
    }

    protected ckeConfig:Map<string, CKEDITOR.config>;

    protected isHover:boolean;
    get IsHover():boolean {
        return this.isHover;
    }

    protected isOpen:boolean;
    get IsOpen():boolean {
        return this.isOpen;
    }

    protected isCreatedEditor:boolean;


    public init(){
        this.findLiveSection();
        this.registerEvents();

        //Register ckeditor configs
        this.registerCkeConfig();

        //Add text in editable element
        this.$element.find('._liveEditor-content').append(this.config.data);
    }

    public edit = () => {
        if(!this.isCreatedEditor){
            this.createEditor();
        }

        this.liveSection.Locked = true;

        //Show ckeditor
        this.$element.addClass('_live-editing');
        $('#modal__' + this.EditorId).removeClass('_hidden');

        this.isOpen = true;

        //hide text
        //$(this.services['$element']).find('p').css('color', 'transparent');
    }

    public save = () => {
        var $angular_response = this.services['$angular_response'];
        var cke = $('#cke_' + this.EditorId);

        var newData = CKEDITOR.instances[this.EditorId].getData();

        newData = newData.replace('&nbsp;', '');
        console.log(newData);

        var that = this;
        $angular_response().post('fragments/save',{
            '__identifier': this.config.save.identifier,
            '__table': this.config.save.table,
            '__id': this.config.save.id,
            'text': newData
        }).success(function(){

            //if(angular.isDefined(Materialize)){
            //    Materialize.toast('Texte sauvegardé !', 3000);
            //}

            that.config.data = newData;
            that.$element.find('._liveEditor-content').html(newData);
            CKEDITOR.instances[that.EditorId].setData(newData);
            that.back();

        });
    }

    public back = () => {
        var editor = CKEDITOR.instances[this.EditorId];

        if(editor.commands.maximize.state == 1)
            editor.execCommand('maximize');

        $('#modal__' + this.EditorId).addClass('_hidden');
        this.$element.removeClass('_live-editing');

        //Hide ckeditor
        //$('#cke_' + this.EditorId).removeClass('cke--active');

        //Show element text
        //this.$element.find('p').css('color', 'inherit');
        //this.$element.find('._liveEditor-edit').css('diplay', 'block');

        //reset editor data
        CKEDITOR.instances[this.EditorId].setData(this.config.data);

        setTimeout(() => {
            this.liveSection.Locked = false;
        }, 50);

        this.isOpen = false;
    }

    protected registerCkeConfig() {
        this.ckeConfig = new Map<string, CKEDITOR.config>();

        this.ckeConfig.set('text', {
            format_tags: 'p;h1;h2;h3;pre',
            removeDialogTabs: 'image:advanced;link:advanced',
            resize_enabled: false,
            allowedContent: true,
            extraPlugins: 'skimiawidgets',
            removePlugins: 'elementspath',
            skin: 'minimalist',
            enterMode: CKEDITOR.ENTER_BR
        });

        this.ckeConfig.set('tinytext', {
            enterMode: CKEDITOR.ENTER_BR
        });
    }

    protected createEditor(){

        //Add modal in body & replace textarea by ckeditor
        $('body').append(this.ModalHtml);

        if(this.ckeConfig.has(this.config.type)) {
            CKEDITOR.replace(this.EditorId, this.ckeConfig.get(this.config.type));
        }
        else{
            CKEDITOR.replace(this.EditorId);
        }

        $('#modal__' + this.EditorId).find('._livemodal-cancel').on('click', () => {
            this.back();
        });

        $('#modal__' + this.EditorId).find('._livemodal-save').on('click', () => {
            this.save();
        });

        /*

        //Create a textarea in body
        $('body').append('<textarea id="' + this.editorId + '">' + this.config.data + '</textarea>');

        //Replace it with ckeditor
        if(this.config.type == 'tinytext') {
            CKEDITOR.replace(this.editorId, this.ckeText);
        }
        else{
            CKEDITOR.replace(this.editorId);
        }


        //Positionning ckeditor over editable div
        this.services['$timeout'](() => {
            var element = $(this.services['$element']);
            var cke = $('#cke_' + this.editorId);
            var cke_top = cke.find('.cke_top');
            var cke_contents = cke.find('.cke_contents');

            cke.css({
                'position': 'absolute',
                'left': element.offset().left,
                'width': element.width()
            });
            cke.css('top', element.offset().top - cke_top.height() - 8);

            cke_contents.css({
                'height': 'auto',
                'color': element.css('color'),
                'line-height': element.find('._liveEditor-content > p').css('line-height'),
                'padding': element.parent().css('padding'),
                'font-size': element.css('font-size'),
                'font-family': element.css('font-family'),
                'font-weight': element.css('font-weight'),
                'text-align': element.css('text-align')
            });

            cke.find('.cke_wysiwyg_div p').css({
                'line-height': element.find('._liveEditor-content > p').css('line-height')
            });

            cke.addClass('cke--active');

            var that = this;
            cke.find('.cke_button__save').on('click', () => {
                that.save();
            });

            cke.find('.cke_button__back').on('click', () => {
                that.back();
            });

        }, 400);

         */

        this.isCreatedEditor = true;
    }

    protected findLiveSection():void{
        if(!this.liveSection){
            var iterator = this.services.$scope;
            var ctrl = null

            while(null != iterator.$parent){
                iterator = iterator.$parent;

                if(null != iterator && 'LiveSection' in iterator){
                    ctrl = iterator.LiveSection;
                }
            }

            this.liveSection = ctrl;
            this.liveSection.Childs.set(this.Name, this);
        }
    }

    protected registerEvents() {
        //open editor on click
        this.services.$element.on('click', () => {
            if(this.liveSection.Opened) {
                this.edit();
            }
        });

        this.services.$element.on('mouseover', () => {
            if(this.liveSection.Opened){
                this.isHover = true;

                if(this.services.$scope.$$phase != '$apply')
                    this.services.$scope.$apply();
            }
        });

        this.services.$element.on('mouseleave', () => {
            this.isHover = false;

            if(this.services.$scope.$$phase != '$apply')
                this.services.$scope.$apply();
        });
    }
}

liveEditApp.bindComponent(LiveEditable);