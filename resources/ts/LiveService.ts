/// <reference path="pagesApp.ts"/>

class LiveService extends Skimia.Angular.Service {

    protected sectionsRoot:Map<string,LiveSectionManager> = new Map<string,LiveSectionManager>();

    get SectionsRoot():Map<string,LiveSectionManager> {
        return this.sectionsRoot;
    }

    protected liveCharm:LiveCharmManager;

    set LiveCharm(livecharm:LiveCharmManager) {
        this.liveCharm = livecharm;
    }



    public registerSection(section:LiveSectionManager):void {

        if(!this.sectionsRoot){
            this.sectionsRoot = new Map<string, LiveSectionManager>();
        }

        if(section.Parent == null) {
            this.sectionsRoot.set(section.Name, section);
        }
    }

    public disableSections() {
        this.sectionsRoot.forEach((section:LiveSectionManager) => {
            section.Disabled = true;
        });
    }

    public enableSections() {
        this.sectionsRoot.forEach((section:LiveSectionManager) => {
            section.Disabled = false;
        });
    }

    public closeSections() {
        this.sectionsRoot.forEach((section:LiveSectionManager) => {
            section.Events.close();
        });
    }

    public openCharmItems(section:LiveSectionManager) {
        this.liveCharm.SelectedSection = section;
        this.liveCharm.IsOpenItems = true;
        this.liveCharm.IsOpenSections = false;
    }

    public openCharmSections() {
        this.liveCharm.IsOpenSections = true;
        this.liveCharm.IsOpenItems = false;
    }

    public refreshCharm() {
        var charmScope = this.liveCharm.services.$scope;

        if(charmScope.$$phase != '$apply') {
            charmScope.$apply();
        }
    }
}

//liveEditApp.bindService('LiveService', LiveService);
liveEditApp.module.service('liveService', LiveService);