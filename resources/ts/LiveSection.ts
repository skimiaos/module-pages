/// <reference path="pagesApp.ts"/>
/// <reference path="LiveService.ts"/>

declare var Materialize:{toast:(string,int) => void;}

interface ILiveSectionScope extends ng.IScope {
    $parent;
    LiveSection: LiveSectionManager;
}


class LiveSection extends Skimia.Angular.Component{
    selector = 'liveSection';
    restrict = 'A';
    manager = {
        alias: 'LiveSection',
        class: LiveSectionManager,
        bindings: {
            name: '@liveSection'
        }
    };
    view = {
        templateCache: 'LiveSection.html',
        transclude: true
    }
}
class LiveSectionManager extends Skimia.Angular.Manager{
    static injectables = ['$scope', '$element', 'liveService'];

    public services:{
        $element:JQuery;
        $scope:ILiveSectionScope;
        liveService:LiveService;
    }

    protected events;

    get Events() {
        return this.events;
    }

    protected name:string;
    protected parent:LiveSectionManager;
    protected childs:Map<string,LiveSectionManager|LiveEditableManager>
    protected isHover:boolean;
    protected isOpen:boolean;
    protected isDisabled:boolean;
    protected isLocked:boolean;

    get Name():string{
        return this.name;
    }

    get Parent():LiveSectionManager{
        return this.parent;
    }

    get Childs():Map<string,LiveSectionManager|LiveEditableManager>{
        return this.childs;
    }

    get ChildsArray():Array<LiveSectionManager|LiveEditableManager> {
        var array = new Array<LiveSectionManager|LiveEditableManager>();

        this.childs.forEach((child) => {
            array.push(child);
        });

        return array;
    }

    get Hover():boolean {
        return this.isHover;
    }

    get Opened():boolean {
        return this.isOpen;
    }

    get Disabled():boolean {
        return this.isDisabled;
    }

    set Disabled(disabled:boolean){
        this.isDisabled = disabled;
    }

    get Locked():boolean {
        return this.isLocked;
    }

    set Locked(locked:boolean){
        this.isLocked = locked;
    }

    /**
     *
     */
    public init(){
        this.isOpen = false;
        this.isDisabled = true;
        this.isLocked = false;
        this.isHover = false;

        this.parent = null;
        this.findParent();
        this.childs = new Map<LiveSectionManager|LiveEditableManager>();

        this.registerEvents();
        this.registerStyles();
        this.services.liveService.registerSection(this);
    }

    public edit(){
        this.services.liveService.closeSections();
        this.services.liveService.disableSections();
        this.isDisabled = false;
        this.isOpen = true;

        var live = this.services.liveService;
        live.openCharmItems(this);
        live.refreshCharm();
    }

    public close(){
        this.isOpen = false;
        this.services.liveService.enableSections();
        this.services.liveService.openCharmSections();
    }

    protected registerStyles(){

        if(this.services.$element.css("position") == "static"){
            this.services.$element.css("position","relative");
            console.warn('la live section "'+this.name+'" doit avoir l\'attribut css position défini pour fonctionner correctement elle a été mise à la volée',this.services.$element);
        }

    }

    protected registerEvents() {

        var Events = {
            hover: () => {
                if(!this.isOpen && !this.isDisabled){
                    this.services.$element.addClass('_hover');
                    this.isHover = true;
                    this.services.liveService.refreshCharm();

                    //element.parents('[live-section]').removeClass('_hover');

                    event.stopPropagation();
                }
            },
            leave: () => {
                event.stopPropagation();

                if(!this.isOpen && !this.isDisabled) {
                    this.services.$element.removeClass('_hover');
                    this.isHover = false;
                    this.services.liveService.refreshCharm();
                }
            },
            click: () => {
                if(!this.isDisabled) {
                    this.edit();

                    var element = this.services.$element;
                    element.addClass('_editing _hover');

                    if(element.offset().top > 50){
                        element.find('._live-element').addClass('_top');
                    }
                    else{
                        element.find('._live-element').removeClass('_top');
                    }
                }
            },
            clickout: ($event:JQueryEventObject = null) => {
                event.stopPropagation();

                if(this.isOpen && !this.isLocked){
                    this.events.close();
                }
            },
            close: () => {
                if(!this.isLocked){
                    event.stopPropagation();
                    this.close();
                    this.isHover = false;
                    this.services.liveService.refreshCharm();

                    var element = this.services.$element;
                    element.removeClass('_editing _hover');
                    element.find('._live-element').removeClass('_top');
                }
            }
        };

        this.events = Events;

        this.services.$element.on('mouseover', Events.hover);
        this.services.$element.on('mouseleave', Events.leave);
        this.services.$element.on('clickoutside', Events.clickout);
        this.services.$element.on('click', Events.click);
    }

    protected findParent() {
        var iterator:ILiveSectionScope = this.services.$scope;

        while(null != iterator.$parent){
            iterator = iterator.$parent;

            if(null != iterator && 'LiveSection' in iterator){
                this.parent = iterator.LiveSection;
            }
        }
    }

}

liveEditApp.bindComponent(LiveSection);