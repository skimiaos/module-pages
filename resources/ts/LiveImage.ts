/// <reference path="pagesApp.ts"/>

declare var Materialize:{toast:(string,int) => void;}
declare var Browse;

interface JQuery{
    leanModal(): void;
    openModal(): void;
}

/**
 * LiveEditable
 */
class LiveImage extends Skimia.Angular.Component{
    selector = 'liveImage';
    restrict = 'A';
    manager = {
        alias: 'LiveImage',
        class: LiveImageManager,
        bindings: {
            config: '=liveImage',
            src: '@ngSrc',
            description: '@alt'
        }
    };
}
class LiveImageManager extends Skimia.Angular.Manager{
    static injectables = ['$scope', '$element', '$timeout', '$angular_response'];

    config:{
        table:string;
        id:string;
        identifier:string;
    };
    src:string;
    description:string;

    get Type():string {
        return 'image';
    }

    get Name():string {
        return this.config.identifier;
    }

    get EditorId():string {
        return this.config.table.replace(/\W/g, '')
            + this.config.id.replace(/\W/g, '')
            + this.config.identifier.replace(/\W/g, '');
    }
    get ModalHtml():string {
        return `
            <div class="_livemodal" id="modal__` + this.EditorId + `">
                <div class="_livemodal-box">
                    <header>
                        Edition de l'image ` + this.Name + `
                    </header>
                    <main>
                        <form class="_livemodal-imgform">
                            <div class="_livemodal-image">
                                <a href="#" class="">Choisir une image</a>
                                <img src="http://placehold.it/1920x1080" alt="">
                            </div>
                            <input type="text" class="fileinput">
                            <label>Description de la photo (Ceci aide au référencement)</label>
                            <input type="text" name="description">
                        </form>
                    </main>
                    <footer>
                        <a href="#" class="_livemodal-cancel">Annuler</a>
                        <a href="#" class="_livemodal-save">Enregistrer</a>
                    </footer>
                </div>
                <div class="_livemodal-overlay"></div>
            </div>
        `;
    }
    get ModalSelector():JQuery {
        return $('#modal__' + this.EditorId);
    }

    protected liveSection:LiveSectionManager;

    /**
     *
     */
    public init(){
        this.findLiveSection();
        this.registerEvents();


        /*var element = $(this.services['$element']);
        element.parent().find('.fileinput')
            .on('input', () => {
                element.attr('src', element.parent().find('.fileinput').val());

                element.parent().find('.modal').leanModal();
                element.parent().find('.modal').openModal();
                element.parent().find('._liveImage-save').on('click', () => {
                    this.save();
                });

                element.parent().find('._liveImage-back').on('click', () => {
                    this.back();
                });
            })

        //open editor on click
        element.on('click', () => {
            this.edit();
        });

        element.on('mouseover', () => {
            element.addClass('_hover');
        });

        element.on('mouseleave', () => {
            element.removeClass('_hover');
        });*/
    }

    public edit = () => {
        Browse(this.services['$element'], $('#modal__' + this.EditorId));
    }

    public save = () => {
        var element = this.services['$element'];
        var $scope = this.services['$scope'];
        var $angular_response = this.services['$angular_response'];
        var that = this;

        $angular_response().post('fragments/save',{
            '__identifier': this.config.identifier,
            '__table': this.config.table,
            '__id': this.config.id,
            'src': this.ModalSelector.find('.fileinput').val(),
            'description': this.ModalSelector.find('input[name="description"]').val()
        }).success(function(){

            //if(angular.isDefined(Materialize)){
            //    Materialize.toast('Image sauvegardée !', 3000);
            //}

            element.attr('src', that.ModalSelector.find('.fileinput').val());
            that.src = element.attr('src');
            that.description = that.ModalSelector.find('input[name="description"]').val();
            that.back();
        });
    }

    public back = () => {
        var element = $(this.services['$element']);
        //element.attr('src', this.src);
        this.ModalSelector.find('.fileinput').val(this.src);
        this.ModalSelector.find('input[name="description"]').val(this.description);
        this.ModalSelector.addClass('_hidden');
    }

    protected createModal() {
        $('body').append(this.ModalHtml);

        var imgSrc:string = this.services['$element'].attr('ng-src');
        this.ModalSelector.find('._livemodal-image img').attr('src', imgSrc);
        this.ModalSelector.find('.fileinput').val(imgSrc);
        this.ModalSelector.find('input[name="description"]').val(this.description);

        this.ModalSelector.find('._livemodal-cancel').on('click', (event) => {
            event.stopPropagation();
            this.back();
        });

        this.ModalSelector.find('._livemodal-save').on('click', (event) => {
            event.stopPropagation();
            this.save();
        });

        this.ModalSelector.find('._livemodal-image a').on('click', (event) => {
            event.stopPropagation();
            this.edit();
        });

        this.ModalSelector.find('.fileinput').on('input', () => {
            this.ModalSelector.find('._livemodal-image img')
                .attr('src', this.ModalSelector.find('.fileinput').val());
        });
    }

    protected findLiveSection():void{
        if(!this.liveSection){
            var iterator = this.services.$scope;
            var ctrl = null

            while(null != iterator.$parent){
                iterator = iterator.$parent;

                if(null != iterator && 'LiveSection' in iterator){
                    ctrl = iterator.LiveSection;
                }
            }

            this.liveSection = ctrl;
            this.liveSection.Childs.set(this.Name, this);
        }
    }

    protected registerEvents() {
        var elem = this.services['$element'];

        elem.on('click', () => {
            if(this.liveSection.Opened) {
                if (!this.ModalSelector.length) {
                    this.createModal();
                }
                else {
                    this.ModalSelector.removeClass('_hidden');
                }
            }

        });

    }
}
liveEditApp.bindComponent(LiveImage);