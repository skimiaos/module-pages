

    editor.widgets.add( '{{$name}}', {
        button: '{{$widget['button']}}',

        template:'{{str_replace([PHP_EOL,"'"], ['',"\\'"], $widget['dom'])}}',

        editables: {
            @foreach($widget['editables'] as $unique=>$edit)
                {{$unique}}: {
                selector: '{{$edit['selector']}}',
                @if(isset($edit['allowedContent']))allowedContent: '{{$edit['allowedContent']}}'@endif

            }@if(array_search($unique, array_keys($widget['editables'])) != (count($widget['editables']) -1)),@endif

            @endforeach
        },
        allowedContent:
                '{{implode(' ',$widget['allowedContent'])}}[*]',

        requiredContent: '{{$widget['rootNode']['tag']}}({{$widget['rootNode']['class']}})',

        upcast: function( element ) {
            return element.name == '{{$widget['rootNode']['tag']}}' && element.hasClass( '{{$widget['rootNode']['class']}}' );
        },
    parts: {
    @foreach($widget['parts'] as $unique=>$edit)
        {{$unique}}: '{{$edit}}'@if(array_search($unique, array_keys($widget['parts'])) != (count($widget['parts']) -1)),@endif


    @endforeach
    }
    } );
