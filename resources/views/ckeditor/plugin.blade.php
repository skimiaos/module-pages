
CKEDITOR.plugins.add( '{{$name}}', {

        // Simple Box widget code.

        requires: 'widget',

        icons: '{{$name}}',

        init: function( editor ) {

            {{$widgets}}

        }

} );
