
<div class="col s12" @if(isset($field['ng-show']))ng-show="{{$field['ng-show']}}"@endif>
    <label for="{{$name}}">{{$field['label']}}</label>
    <textarea id="wysiwyg-{{$name}}" @block('form.row.widget.ng-model')
              ng-model="form['{{$name}}']" @endshow name="{{$name}}" {{$field['dsbl']}} @block('form.row.widget.attributes')@endshow

              ckeditor="{extraPlugins:'lineutils,clipboard,widget,skimiabox';}">
    </textarea>


</div>
    <style>
        .cke_chrome {
            visibility: inherit !important;
        }
    </style>

