<div os-flex="1" @if(isset($field['ng-show']))ng-show="{{$field['ng-show']}}"@endif>
    <label>{{isset($field['label'])?$field['label']:''}}</label>


    <os-select name="{{$name}}" ng-init="form['{{$name}}'] = null"
               @block('form.row.widget.attributes')@endshow
               @block('form.row.widget.ng-model') ng-model="<?php echo AngularFormHelper::getNgModel($name)?>" @endshow

               @block('form.row.widget.select-options')
               options="form['componentPageLink.{{$field['sysName']}}.selects']"
               @endshow
               placeholder="Choisissez une option">
        {{isset($field['label']) ? $field['label'] : ucfirst($name)}}
    </os-select>


</div>


