{{-- AceEditor config page
  Implements blocks:
    - page.content (backend.layout.config-page)
  --}}

@extends('skimia.backend::layouts.config-page')

@block('page.actions')
<a class="waves-effect waves-light btn-flat transparent white-text"
   ng-click="save(form,$event,$event)">
    <i class="os-icon-floppy-1"></i>
    Enregistrer
</a>

@endblock
@block('page.content')

    @block('grid.cols.1')
@block('form.submit')

@endoverride
        @include(Config::get('skimia.angular::templates.form'), array('fields'=>$fields,'form'=>$form,'angular_submit'=> $angular_submit))
    @endoverride

    @block('grid.cols.2')
        <div style="min-height:300px; position: absolute; top: 0; left: 0; right: 0; bottom: 0;"
             class="col-md-6"
             ui-ace="aceOptions" ng-model="filedemo"></div>
    @endoverride

    @include('skimia.backend::partials.grids.columns', ['cols' => ['2', '3']])

@endoverride



@Controller
@AddDependency('$dataSource')

//<script>
    $source = $dataSource.source('kvp','ace-editor');

    $scope.aceOptions = {
        useWrapMode : true,
        fontSize:    $source.get('fontSize'),
        theme:$source.get('theme'),
        mode: 'css',
        showGutter: $source.get('showGutter'),
        rendererOptions:{
            showInvisibles: $source.get('showInvisibles')
        },
        onLoad:function(_editor){
            var _session = _editor.getSession();
            $scope._editor = _editor;
            $scope._editor.setOption('highlightActiveLine' , $source.get('highlightActiveLine'));
            _session.setOption('wrap' , $source.get('wrap'));
            _session.setOption('tabSize' , $source.get('tabSize'));
            _session.setOption('useSoftTabs' , $source.get('useSoftTabs'));
            _editor.$blockScrolling = Infinity;
        }
    };

    $scope.save = function($data,$event){

        var deferred = $q.defer();
        $angular_response($scope,$event).post('{{$saveUrl}}',$data).
            success(function(data, status, headers, config) {
                    $source.set('theme',$scope.form.<?php echo a_field_id('theme',$fields); ?>);
                    $source.set('fontSize',$scope.form.<?php echo a_field_id('font_size',$fields); ?>);
                    $source.set('showInvisibles',$scope.form.<?php echo a_field_id('invisibles',$fields); ?>);
                    $source.set('highlightActiveLine',$scope.form.<?php echo a_field_id('active_line',$fields); ?>);
                    $source.set('showGutter',$scope.form.<?php echo a_field_id('gutter',$fields); ?>);
                    $source.set('wrap',$scope.form.<?php echo a_field_id('wwrap',$fields); ?>);
                    $source.set('tabSize',$scope.form.<?php echo a_field_id('tab_size',$fields); ?>);
                    $source.set('useSoftTabs',$scope.form.<?php echo a_field_id('indent_by_tab',$fields); ?>);

                    //console.log($source);
                    deferred.resolve(data);
            });
        return deferred.promise;
    };

    controller_handler.notifyScopeInitialised = function($scope){
        var _renderer = $scope._editor.renderer;
        var _session = $scope._editor.getSession();
        $scope.name_desc.name = $scope.name;
        $scope.name_desc.description = $scope.description;

        $scope.$watch('form.<?php echo a_field_id('font_size',$fields); ?>',function(newValue){
            _renderer.setOption('fontSize' , Number(newValue));
        });

        $scope.$watch('form.<?php echo a_field_id('invisibles',$fields); ?>',function(newValue){
            _renderer.setOption('showInvisibles' , newValue);
        });

        $scope.$watch('form.<?php echo a_field_id('active_line',$fields); ?>',function(newValue){
            $scope._editor.setOption('highlightActiveLine' , newValue);
        });

        $scope.$watch('form.<?php echo a_field_id('gutter',$fields); ?>',function(newValue){
            _renderer.setOption('showGutter' , newValue);
        });

        $scope.$watch('form.<?php echo a_field_id('wwrap',$fields); ?>',function(newValue){
            _session.setOption('wrap' , newValue);
        });

        $scope.$watch('form.<?php echo a_field_id('tab_size',$fields); ?>',function(newValue){
            _session.setOption('tabSize' , Number(newValue));
        });

        $scope.$watch('form.<?php echo a_field_id('indent_by_tab',$fields); ?>',function(newValue){
            _session.setOption('useSoftTabs' , !newValue);
        });

        $scope.$watch('form.<?php echo a_field_id('theme',$fields); ?>',function(newValue){
            var gutter = _renderer.getOption('showGutter');
            $scope.aceOptions.theme = newValue;
            _renderer.setOption('showGutter' , gutter);
        });

    }


    $scope.filedemo = "form, fieldset, h5, h6, pre, blockquote, ol, dl, dt, dd, address, dd, dtm, div, td, th, hr {\n"+
    "margin: 0;\n"+
    "padding: 0;\n"+
    "}\n"+
    "\n"+
    "body {\n"+
    "background-color: white;\n"+
    "font: 62.5% Helvetica, Arial, Tahoma, Verdana, Helvetica, sans-serif;\n"+
    "}\n"+
    "\n"+
    "p {\n"+
    "font-size: 12px;\n"+
    "}\n"+
    "\n"+
    "optgroup {\n"+
    "font-style: normal;\n"+
    "color: #333;\n"+
    "}\n"+
    "\n"+
    "option {\n"+
    "color: #000;\n"+
    "}";
//</script>
@EndController