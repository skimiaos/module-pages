@block('form.row.widget.ng-model')
    ng-model="$parent.form['{{$name}}']"
@endoverride

@block('form.row.widget.attributes')
    stop-event
@endoverride

<div live-editable="{type: 'text', data: {{{ json_encode($text,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) }}}, save: {table: '{{$table}}', id: '{{$id}}', identifier: '{{$identifier}}' }}">

</div>
