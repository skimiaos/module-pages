@extends('skimia.angular::form.layout')

@block('page.title')
<?php echo (isset($title) ? addslashes($title): 'Titre de l\'action' ); ?>
@endoverride



@block('page.icon')
<?php echo (isset($icon) ? $icon : 'os-icon-cancel-3' ); ?>
@endoverride

@block('page.actions')
@if(isset($action) && $action != 'create')
    <a class="waves-effect waves-light btn-flat transparent white-text"
       ng-class="{disabled:!activate,green:activate}"
       ng-click="save(form,$event,1)">
        <i class="os-icon-floppy-1"></i>
        {{isset($action) && $action == 'create' ? 'Creer et rester':'Enregistrer'}}
    </a>
@endif
@if(isset($action) && $action == 'create')
    <a class="waves-effect waves-light btn-flat transparent white-text"
       ng-class="{disabled:!activate,orange:activate}"
       ng-click="save(form,$event,2)">
        <i class="os-icon-floppy-1"></i>
        {{ 'Creer'}}
    </a>
@endif

<a class="waves-effect waves-light btn-flat transparent white-text"
   ng-class="{disabled:!activate,red:activate}"
   ng-click="save(form,$event,3)">
    <i class="os-icon-cancel"></i>
    {{ 'Quitter'}}
</a>
@endblock



@block('page.content')
    {{AForm::open('form')}}
        <os-container direction="row">
            {{AngularFormHelper::render($fields['name']['type'],'name', $fields['name']);}}
            {{AngularFormHelper::render($fields['url']['type'],'url', $fields['url']);}}
            {{AngularFormHelper::render($fields['template']['type'],'template', $fields['template']);}}
        </os-container>
        <os-container direction="column">
            {{AngularFormHelper::render($fields['description']['type'],'description', $fields['description']);}}
            @include('skimia.pages::directive.component-bar',['cannot_delete_component'=>true,
                    'components_selector'=>'components',
                    'component_copy'=>false,
                    'component_click'=>'editComponent(component.identifier)'])
        </os-container>

    {{AForm::close()}}
<os-container os-flex="1" style="position:relative" class="pageFullScreen">
<div class="fullScreenSwitch" style="position: absolute;right: 0;top:0;"><a ng-click="fullSwitch();fullscr = !fullscr"><i ng-hide="fullscr" class="os-icon-resize-full-alt"></i><i ng-show="fullscr" class="os-icon-resize-small"></i></a></div>
    <iframe style="border: none;flex: 1;"  ng-src="@{{currentPageUrl}}" >  </iframe>
</os-container>
<style>
    .fullScr{
        position: absolute!important;
        top:0px;
        left: 0px;
        right: 0;
        bottom: 0;
        background-color: white;
        z-index: 999999999;
    }
</style>
@endoverride



@Controller
    @AddDependency('$sce')

    @include('skimia.angular::form.crud.actions.form-js')

    //<script>
    $scope.fullscr = false;
        controller_handler.notifyScopeInitialised = function($scope){

            $scope.activate = true;

            var url = $scope.form.url;

            if(url[0] != '/'){
                url = '/' + url;
            }

            $scope.currentPageUrl = $sce.trustAsResourceUrl('{{url()}}'+ url);


        };

        $scope.fullSwitch = function (  ) {
            $('.pageFullScreen').toggleClass('fullScr');
        };
    //</script>


@EndController

