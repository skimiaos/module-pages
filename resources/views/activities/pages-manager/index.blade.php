{{-- Pages Manager homepage template
  Implements blocks:
    - sidenav.items (backend.partials.sidenav.default)
  --}}

{{-- Parent layout --}}
@extends('skimia.backend::layouts.activity-home')

{{-- Sidenav items implementation --}}
@block('sidenav.items')
    <?php $items = Hook::call('activities.pages_manager.sidenav-item')['items']; ?>
    @foreach($items as $item)
        @if(isset($item['type']) && $item['type'] == 'menu')
            <os-sidenav-item
                    {{isset($item['color']) ? 'color="'.$item['color'].'"':'';}}
                    ng-class="{'active':!$state.includes('{{$item['state']}}')}"
                    label="{{$item['name']}}"
                    icon="{{$item['icon']}}">

                <os-sidenav-menu>
                    @foreach($item['items'] as $menu_item)

                        <os-sidenav-menu-item
                                name="{{$menu_item['name']}}"
                                icon="{{$menu_item['icon']}}"
                                active="false"
                                ng-class="{'active':$state.includes('{{$menu_item['state']}}')}"
                                ng-click="$state.go('{{$menu_item['state']}}'@if(isset($menu_item['stateParams'])),{{{json_encode($menu_item['stateParams'])}}} @endif)">{{$menu_item['description']}}</os-sidenav-menu-item>

                    @endforeach
                </os-sidenav-menu>
            </os-sidenav-item>

        @elseif(isset($item['type']) && $item['type'] == 'state')
        <os-sidenav-item
                {{isset($item['color']) ? 'color="'.$item['color'].'"':'';}}
                ng-class="{'active':!$state.includes('{{$item['state']}}')}"
                class="@{{activity.bg}}"
                label="{{$item['name']}}"
                icon="{{$item['icon']}}"
                ng-click="$state.go('{{$item['state']}}'@if(isset($item['stateParams'])),{{{json_encode($item['stateParams'])}}} @endif)">
        </os-sidenav-item>
        @endif
    @endforeach
@endoverride