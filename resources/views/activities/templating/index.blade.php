{{-- Templating homepage template
  Implements blocks:
    - page.sidenav (backend.layouts.default)
    - page.activity.content (backend.layouts.default)
  --}}

{{-- Parent layout --}}
@extends('skimia.backend::layouts.activity-home')

{{-- Sidenav partial include --}}
@block('page.sidenav')
    @include('skimia.backend::partials.sidenav.browser')
@endoverride

{{-- Page activity content --}}
@block('page.activity.content')
    <crud-fm types='typesOptions' events="events"></crud-fm>
@endoverride


{{-- Angular Controller --}}
@Controller
    $scope.typesOptions = {{stripcslashes(json_encode($types,JSON_FORCE_OBJECT))}};
    $scope.files = {};
    angular.forEach($scope.typesOptions,function($value, $key){
        $angular_response().get($value['list']).success(function(data){
            $scope.files[$key] = data;
            $scope.typesOptions[$key]['__scope'] = $scope.files;
        });
    });

    $scope.events = {
        assets: {
            open:function(file){alert(file.info)}
        }
    };
@EndController
