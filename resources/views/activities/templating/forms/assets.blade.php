@ImportGlobController(TemplatingEditorController)
    <os-container direction="column" class="layout-crud">

        <os-container direction="row" class="layout-crud--header green">
            <os-input name="path" type="text"
                    ng-model="Files[filid]['form']['path']"
                    ng-init="form['path'] = (form['path'] ? form['path']:null)">Nom de fichier</os-input>
        </os-container>

        <os-container direction="row" class="layout-crud--header-actions green">
            <button class="btn-flat transparent waves-effect waves-light" ng-click="saveFile(filid)">
                {{isset($saveBtn)?$saveBtn:'Enregister'}}
                <i class="mdi-content-save left"></i>
            </button>
            <button class="btn-flat transparent waves-effect waves-light" ng-click="deleteFile(filid)">
                Supprimer
                <i class="os-icon-trash-2 left"></i>
            </button>
        </os-container>

        <div os-flex="1" ui-ace="$dataModel.aceOptions" ng-model="Files[filid]['form']['file']"></div>
    </os-container>
@EndImportGlobController

@GlobController('TemplatingEditorController')
    //<script>
        $scope.$dataModel = $dataSource.model('ace');
        console.log($scope.$dataModel.aceOptions.fontSize);
    //</script>
@EndGlobController
