@block('form.start')
    {{AForm::open('Files[filid][\'form\']')}}
@endblock

@block('form.row.widget.ng-model')ng-model="Files[filid]['form']['{{$name}}']"@endoverride
@block('form.row.widget.select-options')options="Files[filid]['form']['{{$name}}*selectItems']"@endoverride

@block('form.row.widget.content')
    @block('form.row.widget.content.'.$field['type'])
        {{AngularFormHelper::render($field['type'],$name, $field);}}
    @endshow
@endoverride

@block('path.attributes')os-flex="50%"@endoverride
@block('description.attributes')os-flex="50%"@endoverride

@block('field.path.row.before')<os-container direction="row" class="layout-crud--header green">@endoverride
@block('field.description.row.after')
    </os-container>
    <os-container direction="row" class="layout-crud--header-actions green">
        <button class="btn-flat transparent waves-effect waves-light" ng-click="saveFile(filid)">
            {{isset($saveBtn)?$saveBtn:'Enregister'}}
            <i class="mdi-content-save left"></i>
        </button>
        <button class="btn-flat transparent waves-effect waves-light" ng-click="deleteFile(filid)">
            Supprimer
            <i class="os-icon-trash-2 left"></i>
        </button>
    </os-container>
@endoverride

@block('form.submit')@endblock

@block('form.row.label')@endblock
@block('form.row.errors')@endblock

@block('form.end')
    {{AForm::close()}}
@endblock

@block('form.end.after')
    @include('skimia.pages::directive.component-bar')
@endblock





@ImportGlobController(TemplatingEditorController)
    <os-container direction="row">
        <os-container direction="column" class="layout-crud">
            @include('skimia.form::global',array('fields'=>$fields,'form'=>$form))

            <div os-flex="1" ui-ace="$dataModel.aceOptions" ng-model="Files[filid]['form']['file']"></div>

        </os-container>
        <os-container direction="column" class="os-toolbox">
            <div class="os-toolbox-tab">
                <i class="os-icon-tools"></i>
            </div>
            <h6>Toolbox</h6>
            <div class="os-toolbox-selector">
                <div class="input-field col s12">
                    <select>
                        <option value="" disabled selected>Components</option>
                    </select>
                    <label>Catégories</label>
                </div>
            </div>
            <div class="os-toolbox-items">
                <div class="os-toolbox-item tooltipped"
                     ng-repeat="(key,component) in component_types"
                     ng-click="bindComponent(key)"
                     data-position="left"
                     data-delay="50"
                     data-tooltip="@{{ component.description }}">
                    <i class="@{{ component.icon }}"></i>
                    <p>@{{ component.name }}</p>
                </div>
            </div>
        </os-container>
    </os-container>
@EndImportGlobController

@GlobController('TemplatingEditorController')
    //<script>
        $scope.$dataModel = $dataSource.model('ace');
        console.log($scope.$dataModel.aceOptions.fontSize);

        //Toolbox
        $('.os-toolbox-selector select').material_select();
        setTimeout(function(){$('.tooltipped').tooltip({delay: 50})}, 1);
        console.log($scope);

        var toolboxTab = $('.os-toolbox-tab');
        toolboxTab.on('click', function(){
            if($('.os-toolbox').hasClass('os-toolbox--active')){
                $('.os-toolbox').removeClass('os-toolbox--active');
                $('.layout-crud').removeClass('layout-crud--toolbox');
                toolboxTab.find('i').removeClass('os-icon-right');
                toolboxTab.find('i').addClass('os-icon-tools');
            }
            else{
                $('.os-toolbox').addClass('os-toolbox--active');
                $('.layout-crud').addClass('layout-crud--toolbox');
                toolboxTab.find('i').removeClass('os-icon-tools');
                toolboxTab.find('i').addClass('os-icon-right');
            }

        });
    //</script>
@EndGlobController
