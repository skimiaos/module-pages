
<os-tabset class="">

    <os-tab ng-repeat="filid in openFiles"
            close="closeOpenFile(filid)"
            active="Files[filid].active"
            modified="Files[filid].__modified"
            heading="@{{Files[filid].path}}">
        <i class="fa fa-@{{ Files[filid].__modified ? 'circle' : files_types[Files[filid].__file_type].icon}}"></i>
        <div ng-include="files_types[Files[filid].__file_type].form"
             ng-init="form=$scope.Files[filid].form"
             class="fill-height-form">

        </div>

    </os-tab>

</os-tabset>



