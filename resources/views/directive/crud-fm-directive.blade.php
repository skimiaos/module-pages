//<script>
var crudFM = angular.module('crudFileManager', ['backend','ngClipboard','oc.modal']);


crudFM.controller('editComponentCtrl',['$scope', '$ocModal','$angular_response', function(scope, $ocModal, $angular_response) {

scope.closeDialog = function() {
    alert('close');
};


scope.asubmit = function(){
    $uri = replace('{{route('skimia.pages::components.edit',[
                                'class'=>':class',
                                'id'=>':id',
                                'identifier'=>':identifier'
                            ])}}',{
        ':class' : scope.classe,
        ':id': scope.id,
        ':identifier':scope.identifier
    } );
    $angular_response().post($uri,scope.form).success(function($up_file) {

        $ocModal.close('componentEdit', $up_file, scope.form._identifier);
    });

};
}]);

crudFM.directive('crudFm',['$state','$timeout', '$compile', '$ocModal', function($state,$timeout,$compile,$ocModal){

    return {
        restrict: 'E',
        scope: {
            files_types: '=types',
            events: '='
        },
        templateUrl : '{{route('skimia.pages::crud-tpl')}}',
        controller: function($scope,$api,$http,$mdBottomSheet,$mdDialog,messageCenterService,$angular_response){


            $scope.$state = $state;
            //handle Ctrl+S
            //Attention si la directive est utilisée deux fois

           $(window).bind('keydown', function(event) {
                if (event.ctrlKey || event.metaKey) {
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                        case 's':
                            event.preventDefault();
                                angular.forEach($scope.Files,function(value,key){
                                    if(value.active){
                                        $scope.saveFile(key);
                                        return;
                                    }
                                });

                        break;

                    }
                }
            });

            $scope.Files = {};
            $scope.openFiles = [];
            $thiscope = $scope;
            $scope.component_types = {{stripcslashes(json_encode(ComponentManager::describeComponents(),JSON_FORCE_OBJECT))}};

            $scope.openFile = function($type,$file){
                var $fileid = $type+'*'+$file['id'];

                if(angular.isDefined($scope.Files[$type+'*'+$file['id']])){
                    $scope.Files[$type+'*'+$file['id']]['active'] = true;
                    return;
                }
                $uri = replace($scope.files_types[$type].get_edit,{':id' : $file['id']} );
                $angular_response().get($uri).success(function($resp){

                    if(angular.isDefined($resp['ack']) && $resp['ack'] == false) return;

                    $scope.openFiles.push($fileid);
                    $scope.Files[$type+'*'+$file['id']] = angular.extend({'__file_type': $type,'__modified':false,'__file_id':$fileid },$file);

                    $scope.Files[$type+'*'+$file['id']]['components'] = $resp['components'];

                    $scope.Files[$type+'*'+$file['id']]['form'] = $resp['form'];

                    $scope.Files[$type+'*'+$file['id']]['path'] = $scope.Files[$type+'*'+$file['id']]['path'].replace(/^.*[\\\/]/, '');

                    $timeout(function(){
                        $scope.Files[$type+'*'+$file['id']]['active'] = true;
                    },400);

                    var first = false;
                    var watch = $scope.$watchCollection('Files[\''+$type+'*'+$file['id']+'\'].form',function(newValue,oldValue){

                        if(!angular.isDefined($scope.Files[$type+'*'+$file['id']])){
                            watch();
                            return;
                        }

                        if(first){
                            $scope.Files[$type+'*'+$file['id']]['__modified'] = true;
                            watch();
                        }
                        first = true;
                    },true);

                });
            };

            $scope.closeOpenFile = function($fileid, $force){
                if(!angular.isDefined($force)) $force = false;
                if($force || !$scope.Files[$fileid].__modified || confirm('Voulez vous fermer ce fichier les changements non sauvegardés seront perdus')){
                    $scope.openFiles.splice($scope.openFiles.indexOf($fileid),1);
                    delete $scope.Files[$fileid];
                }
            }

            $scope.reloadResourceList = function($file_type){

                //console.log($scope.files_types[$file_type].__scope);


                $angular_response().get($scope.files_types[$file_type].list).success(function(data){
                    $scope.files_types[$file_type].__scope[$file_type] = data;

                });
            };

            $scope.saveFile = function($fileid){
                $file = $scope.Files[$fileid];
                $uri = replace($scope.files_types[$file.__file_type].edit,{':id' : $file['id']} );
                $angular_response().post($uri,$file.form).success(function($up_file){

                    $scope.Files[$fileid] = angular.extend($scope.Files[$fileid],$up_file);
                    $scope.Files[$fileid]['__modified'] = false;
                    $scope.Files[$fileid]['path'] = $scope.Files[$fileid]['path'].replace(/^.*[\\\/]/, '');

                    $scope.reloadResourceList($file.__file_type);
                    var first = false;
                    var watch = $scope.$watchCollection('Files[\''+$fileid+'\'].form',function(newValue,oldValue){

                        if(!angular.isDefined($scope.Files[$fileid])){
                            watch();
                            return;
                        }
                        if(first){
                            $scope.Files[$fileid]['__modified'] = true;
                            watch();
                        }
                        first = true;
                    },true);



                });

            };

            $scope.deleteFile = function($fileid){
                if(confirm('Voulez vous vraiment supprimer ce fichier ?')){
                    $file = $scope.Files[$fileid];
                    $uri = replace($scope.files_types[$file.__file_type].delete,{':id' : $file['id']} );
                    $angular_response().delete($uri).success(function($up_file){
                        $scope.closeOpenFile($fileid,true);
                        $scope.reloadResourceList($file.__file_type);
                    });
                }
            }

            $scope.bindComponent = function($filecid){
                angular.forEach($scope.Files,function(value,key){
                    if(value.active){
                        $uri = replace('{{route('skimia.pages::components.bind',[
                                    'class'=>':class',
                                    'id'=>':id',
                                    'componentName'=>':componentName'
                                ])}}',{
                            ':class' : $thiscope.Files[key].__file_type,
                            ':id': $thiscope.Files[key].id,
                            ':componentName':$filecid
                        } );
                        $angular_response().get($uri).success(function($resp) {

                            $thiscope.Files[key]['components'].push($resp);
                            $mdBottomSheet.hide();
                        });
                    }
                });
            }

            $scope.unBindComponent = function($fileid,$identifier){
                $uri = replace('{{route('skimia.pages::components.unbind',[
                                'class'=>':class',
                                'id'=>':id',
                                'identifier'=>':identifier'
                            ])}}',{
                    ':class' : $thiscope.Files[$fileid].__file_type,
                    ':id': $thiscope.Files[$fileid].id,
                    ':identifier':$identifier
                } );
                $angular_response().get($uri).success(function($resp) {


                    angular.forEach($thiscope.Files[$fileid]['components'],function($value,$key){
                        if($value.identifier == $resp.identifier)
                        {
                            $thiscope.Files[$fileid]['components'].stackoverflow_remove($value);
                            return;
                        }

                    });

                });


            };

            $scope.editComponent  = function($fileid,$identifier){
                angular.forEach($thiscope.Files[$fileid].components,function($value){
                    if($value.identifier == $identifier){


                        /*
                        var parentEl = angular.element(document.body);
                        function DialogController(scope, $mdDialog, form, info) {
                            scope.form = form;
                            scope.info = info;
                            scope.closeDialog = function() {
                                $mdDialog.hide();
                            };


                            scope.asubmit = function(){
                                $uri = replace('{{route('skimia.pages::components.edit',[
                                'class'=>':class',
                                'id'=>':id',
                                'identifier'=>':identifier'
                            ])}}',{
                                    ':class' : $thiscope.Files[$fileid].__file_type,
                                    ':id': $thiscope.Files[$fileid].id,
                                    ':identifier':$identifier
                                } );
                                $angular_response().post($uri,scope.form).success(function($up_file) {

                                    $value.form = $up_file;
                                    $value.identifier = $value.form._identifier;
                                    $mdDialog.hide();
                                });

                            };
                        }

                        var modal = $('#modal-' + $value.identifier);
                        modal.find('.modal-content p').html($value.template);
                        modal.remove();
                        modal.find('.modal-content .btn.green').click(
                                function(){
                            $uri = replace('{{route('skimia.pages::components.edit',[
                                'class'=>':class',
                                'id'=>':id',
                                'identifier'=>':identifier'
                            ])}}',{
                                ':class' : $thiscope.Files[$fileid].__file_type,
                                ':id': $thiscope.Files[$fileid].id,
                                ':identifier':$identifier
                            } );
                            $angular_response().post($uri,scope.form).success(function($up_file) {

                                $value.form = $up_file;
                                $value.identifier = $value.form._identifier;
                                $mdDialog.hide();
                            });

                        });

                        //$timeout(function(){
                            $scope.form = $value.form;
                            var compiled = $compile(modal);
                            var compiledModal = compiled($scope);
                            $('body').append(compiledModal);
                        //}, 3000);

                        modal.openModal();*/

                        $ocModal.open({
                            id: 'componentEdit',
                            template: $value.template,
                            controller: 'editComponentCtrl',
                            cls: 'slide-down',
                            init: {
                                form: $value.form,
                                classe : $thiscope.Files[$fileid].__file_type,
                                id: $thiscope.Files[$fileid].id,
                                identifier:$identifier
                            }
                        })
                    }
                });
            };

            $scope.closeModal = function(identifier){
                $('#modal-' + $value.identifier).closeModal();
            }

            $scope.copyComponent = function($identifier){
                return '@'+'Component(\''+$identifier+'\')';
            }

            $scope.copyComponentInfo = function($identifier){
                messageCenterService.add('info', 'Component Copié, Collez-le !', { timeout: 2000 });
            }

            $scope.aceLoaded = function(_editor) {
                // Options
                _editor.$blockScrolling = Infinity;
            };

            $scope.createFile = function($type, $file, $url){
                var filename = prompt("Creation d'un fichier de type : "+$type, $file.path);
                if (filename != null) {
                    if(filename != $file.path && filename.length > 0){
                        $file.path = filename;
                        $uri = angular.isDefined($url)?$url:$scope.files_types[$type].create;
                        $angular_response().post($uri,$file).success(function($up_file){
                            //console.log($up_file);
                            $scope.openFile($type,$up_file);
                            $scope.reloadResourceList($type);
                        });
                    }
                }
            };

            $scope.events = {

                assets:{
                    open:function($file){
                        $scope.openFile('assets',$file);
                    },
                    create:function($path){
                        $scope.createFile('assets',{path:$path});
                    }
                },
                layout:{
                    open:function($file){
                        $scope.openFile('layout',$file);
                    },
                    create:function($path){
                        $scope.createFile('layout',{path:$path});
                    }
                },
                partial:{
                    open:function($file){
                        $scope.openFile('partial',$file);
                    },
                    create:function($path){
                        $scope.createFile('partial',{path:$path});
                    }
                },
                template:{
                    open:function($file){
                        $scope.openFile('template',$file);
                    },
                    create:function($path){
                        $scope.createFile('template',{path:$path});
                    }
                }
            };
        }
    }
}]);
Object.defineProperty(Array.prototype, "stackoverflow_remove", {
    // Specify "enumerable" as "false" to prevent function enumeration
    enumerable: false,

    /**
     * Removes all occurence of specified item from array
     * @this Array
     * @param itemToRemove Item to remove from array
     * @returns {Number} Count of removed items
     */
    value: function (itemToRemove) {
        // Count of removed items
        var removeCounter = 0;

        // Iterate every array item
        for (var index = 0; index < this.length; index++) {
            // If current array item equals itemToRemove then
            if (this[index] === itemToRemove) {
                // Remove array item at current index
                this.splice(index, 1);

                // Increment count of removed items
                removeCounter++;

                // Decrement index to iterate current position
                // one more time, because we just removed item
                // that occupies it, and next item took it place
                index--;
            }
        }

        // Return count of removed items
        return removeCounter;
    }
});
//</script>