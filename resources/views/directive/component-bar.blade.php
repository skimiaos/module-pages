<component-bar class="component-bar medium">

    <component-position class="component-position tooltipped"
                        ng-repeat="component in {{isset($components_selector)?$components_selector:'Files[filid].components'}}"
                        data-position="bottom"
                        data-delay="50"
                        data-tooltip="@{{ component.info.description }}">
         <i class="@{{ component.info.icon }}"></i>
         <p>@{{component.info.name}}</p>

         @if(!isset($component_copy) || $component_copy == true)
             <i class="mdi-content-content-copy component-position-copy"
                clip-click="copyComponentInfo(component.identifier)"
                clip-copy="copyComponent(component.identifier)"></i>
         @endif

        <i class="os-icon-cog component-position-config"
           ng-click="{{isset($component_click)?$component_click:'editComponent(filid,component.identifier)'}}"></i>

         @if(!isset($cannot_delete_component) || $cannot_delete_component == false)
             <i class="mdi-navigation-close component-position-delete"
                ng-click="unBindComponent(filid,component.identifier)"></i>
         @endif

    </component-position>

</component-bar>

