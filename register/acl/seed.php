<?php


Event::listen('artisan.os.install.acl',function($manager){
    Eloquent::unguard();
    $manager->attachActions(['live_edit','templating','pages','advanced_use']);

    $datetime = Carbon\Carbon::now();

    if(DB::table('roles')->where('name','NoBackend')->count() == 0){

        DB::table('roles')->insert(array(
            'name'       => 'NoBackend',
            'created_at' => $datetime,
            'updated_at' => $datetime,
        ));

    }


    $role = \Orchestra\Model\Role::find(3);
    $manager->attachRole($role);
    $acl = $manager->getAcl();
    $acl->allow('NoBackend', ['live_edit']);
    $acl->allow('Member', ['live_edit']);


    if(User::query()->where('email','front@foobar.com')->get(['id'])->count()  == 0)
    {
        //Create user
        $user = new User;
        $user->email = 'front@foobar.com';
        $user->password = '123456';
        $user->fullname = 'Front User';
        $user->save();
        //Attach member role
        $user->attachRole(3);
    }


},9999);