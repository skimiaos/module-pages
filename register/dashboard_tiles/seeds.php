<?php
Event::listen('skimia.backend::seed.dashboard.default.sections', function($admin){
    if($admin)
        return ['templating'=>'Templating',];
    else
        return ['website'=>'Gestion du Site'];
},1000);
Event::listen('skimia.backend::seed.dashboard.default.tiles', function($admin){
    if($admin)
        return ['templating'=>[
            'templating_index'=>[
                'static_id'=> 'pages.templating',
                'size'=>'large'
            ],
            'page_editor'=>[
                'static_id'=>'page.pages',
                'size'=>'large'
            ]
        ]];
    else
        return ['website'=>[
            'page_editor'=>[
                'static_id'=>'page.pages',
                'size'=>'large'
            ]
        ]];
});