<?php


AngularFormHelper::setTemplate('component-page-link' ,'skimia.pages::form.fields.component-page-link',2000);


AngularFormHelper::setDataViewTransformer('component-page-link',function(&$data,&$value,$key,$field,$form){


    $sysName = $field['sysName'];



    $choices = [];
    $links = \Skimia\Pages\Data\Models\ComponentPage::findForceBySysName($sysName);
    foreach ($links as $link) {
        $choices[$link->id] = $link->alias;
    }


    $data['__deleting'][] = 'componentPageLink.'.$sysName.'.selects';
    $data['componentPageLink.'.$sysName.'.selects'] = $choices;

});