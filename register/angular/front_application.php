<?php

$front_app = Angular::application('front_angular_app');

$front_app->addScript(module_assets('skimia.pages','public/js/check-jquery.js'));

$front_app->addScript('skimia.pages::fragments.ckeditor');
$front_app->addScript(module_assets('skimia.pages','public/js/ckeditor/ckeditor.js'));

$front_app->addScript(module_assets('skimia.pages','js/jquery-click-outside-events.js'));
$front_app->addDependency('manta', module_assets('skimia/manta','/js/manta.js'));
$front_app->addDependency('pages', module_assets('skimia/pages','/js/pages.js'));
$front_app->addScript(module_assets('skimia.pages','js/pages-templates.js'));