<?php


$app = Angular::get(OS_APPLICATION_NAME);

//$app->addDependency('sk-pages');
$app->addDependency('ngCkeditor',module_assets('skimia.pages','js/ng-ckeditor.js'));

$app->addDependency('crudFileManager','skimia.pages::directive.crud-fm-directive');
$app->addTemplateScript(\Assets::js('ckeditor/ckeditor.js','skimia/pages'));

$app->addScript(module_assets('skimia/pages','/js/ng-clip.js'));
//$app->addDependency('ui.bootstrap',module_path('skimia/pages','/js/ui-bootstrap.js'));