<?php


function component_route( $alias, $params = [] ){
    return route('skimia.pages::page-component.'.$alias, $params);
}

function c_route( $tpl_id, $params = [] ){
    return route('skimia.pages::page-component.'.$alias, $params);
}

class AnObj
{
    protected $methods = array();

    public function __construct(array $options)
    {
        $this->methods = $options;
    }

    public function __call($name, $arguments)
    {
        $callable = null;
        if (array_key_exists($name, $this->methods))
            $callable = $this->methods[$name];
        elseif(isset($this->$name))
            $callable = $this->$name;

        if (!is_callable($callable))
            throw new BadMethodCallException("Method {$name} does not exists");

        return call_user_func_array($callable, $arguments);
    }
}

function qs_url($path = null, $qs = array(), $secure = null)
{
    $url = app('url')->to($path == null ?  Request::url():$path, $secure);
    if (count($qs)){

        foreach($qs as $key => $value){
            $qs[$key] = sprintf('%s=%s',$key, urlencode($value));
        }
        $url = sprintf('%s?%s', $url, implode('&', $qs));
    }
    return $url;
}