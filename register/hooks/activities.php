<?php
use Skimia\Backend\Managers\Bridge;

Hook::define('activities.pages_manager.sidenav-item',[
    'items'=>'list|merge'
]);


    Hook::register('activities.pages_manager.sidenav-item',function(Bridge $bridge){
        $acl = App::make('acl');
        $items = [];
        if($acl->can('pages'))
            $items['pages'] = [

                'icon'        => 'os-icon-docs',
                'name'        => 'Pages',
                'type'        => 'state',
                'color'        => 'blue darken-3',
                'state'       => 'pages_manager.pages*list',
                'stateParams' => []
            ];

        if($acl->can('advanced_use'))
            $items['pages_components'] = [

                'icon'        => 'os-icon-spread',
                'name'        => 'Pages Components',
                'type'        => 'state',
                'color'        => 'blue darken-3',
                'state'       => 'pages_manager.pages_components*list',
                'stateParams' => []
            ];

        $bridge->items = $items;
    },1000);

