<?php


Event::listen('skimia.backend::login.succeed',function($acl){
    try{
        if(!$acl->can('backend') && $acl->can('live_edit') ) {

            AResponse::redirect(url())->addMessage(trans('skimia.auth::response.credential.logged-in'));
            return false;
        }
    }catch(\Exception $e){

    }


},10);